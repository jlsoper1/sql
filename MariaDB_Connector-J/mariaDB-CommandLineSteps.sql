
-- mysql --tee=/tmp/sql-admin.log -u root -p

-- show databases;

-- +--------------------+
-- | Database           |
-- +--------------------+
-- | information_schema |
-- | mysql              |
-- | performance_schema |
-- +--------------------+
--
-- use mysql;
-- show tables;

-- grant all privileges on *.* to 'root'@'192.168.100.%' identified by 'my-new-password' with grant option;
--

-- GRANT [type of permission] ON [database name].[table name] TO '[username]'@'localhost';
--
-- ALL PRIVILEGES- allow a MySQL user all access to a designated database (or if no database is selected, across the system)
--         CREATE- allows them to create new tables or databases
--           DROP- allows them to them to delete tables or databases
--         DELETE- allows them to delete rows from tables
--         INSERT- allows them to insert rows into tables
--         SELECT- allows them to use the Select command to read through databases
--         UPDATE- allow them to update table rows
--   GRANT OPTION- allows them to grant or remove other users' privileges


-- select user, host, password, grant_priv, super_priv from mysql.user;
-- update mysql.user set grant_priv='Y', super_priv='Y' where user='root';
-- flush privileges;
--
-- select user();$
-- select current_user();
--
--
--
--
--
--

create user 'eddie'@'%' identified by 'baseball';
grant all privileges on lahman2016.* to 'eddie'@'%';
flush privileges;

show grants for 'eddie'@'%';

drop user 'eddie'@'%';


