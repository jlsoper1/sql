set schema app;

-- A Cartesian Product between two tables, returning all possible combinations of all rows.

select *
from customer c
 cross join orders o
