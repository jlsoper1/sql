set schema app;

--
-- jdbc:derby://localhost:1527/sampleDB;create=true
--

create table customer
(
 customer_id integer not null,
 first_name  varchar(30),
 last_name   varchar(30),
 email       varchar(30),
 address     varchar(30),
 city        varchar(30),
 state          char(2) not null,
 zipcode        char(5) not null
);

alter table customer
 add constraint customer_id_pk primary key (customer_id);

--------------------------

insert into customer values (1,'George','Washington','gwashington@usa.gov','3200 Mt Vernon Hwy','Mount Vernon','VA','22121');
insert into customer values (2,'John','Adams','jadams@usa.gov','1250 Hancock St','Quincy','MA','02169');
insert into customer values (3,'Thomas','Jefferson','tjefferson@usa.gov','931 Thomas Jefferson Pkwy','Charlottesville','VA','22902');
insert into customer values (4,'James','Madison','jmadison@usa.gov','11350 Constitution Hwy','Orange','VA','22960');
insert into customer values (5,'James','Monroe','jmonroe@usa.gov','2050 James Monroe Parkway','Charlottesville','VA','22902');
