set schema app;

select first_name, last_name, order_date, amount
from customer c
 left join orders o
 on c.customer_id = o.customer_id
where order_date is NULL
