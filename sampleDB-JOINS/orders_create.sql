set schema app;

--
-- jdbc:derby://localhost:1527/sampleDB;create=true
--

create table orders
(
 order_id    integer not null,
 order_date  date    not null,
 amount      varchar(10),
 customer_id integer
);

alter table orders
 add constraint order_id_pk
      primary key
       (order_id)
;


insert into orders values (1,'1776-07-04','$234.56',1);
insert into orders values (2,'1760-03-14','$78.50',3);
insert into orders values (3,'1784-05-23','$124.00',2);
insert into orders values (4,'1790-09-03','$65.50',3);
insert into orders values (5,'1795-07-21','$25.50',10);
insert into orders values (6,'1787-11-27','$14.40',9);

-- example of adding foreign key constraint.
--   notice the customer_id values of 10 & 9.  (does not exist in customer table!)

alter table orders
 add constraint customer_id_fk
      foreign key
       (customer_id)
      references customer
       (customer_id)
;

