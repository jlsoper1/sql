set schema app;

-- No such thing as full join --> Apache Derby DB

-- \************************************************************\
--
-- select first_name, last_name, order_date, amount
-- from customer c
--  full join orders o
--  on c.customer_id = o.customer_id
--
-- \************************************************************\

select first_name, last_name, order_date, amount
from customer c
 left join orders o
 on c.customer_id = o.customer_id
where order_date is NULL

UNION

select first_name, last_name, order_date, amount
from customer c
 right join orders o
 on c.customer_id = o.customer_id
where first_name is NULL
