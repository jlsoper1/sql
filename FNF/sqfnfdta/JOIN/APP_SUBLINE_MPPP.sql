SELECT

t1.TSLOB,

t2.DAPPST,
t2.DAPPNO,
t2.DAPPOC,

t1.TPOLST,
t1.TPOLNO,
t1.TPOLOC

FROM SQFNFDTA.B60FP610 t1,
      SQFNFDTA.BDRFP000 t2

WHERE t1.TDRPID = t2.DDRPID AND

       t2.DAPPST = '42' AND
       t2.DAPPNO = '2510102070' AND
       t2.DAPPOC = 0
