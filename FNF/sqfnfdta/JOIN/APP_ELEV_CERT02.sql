SELECT

t2.DAPPST,
t2.DAPPNO,
t2.DAPPOC,

t2.DCVAPP,

t1.TPOLST,
t1.TPOLNO,
t1.TPOLOC,

t1.TELEYN,
t1.TELEVC,
t1.TRTMET

FROM SQFNFDTA.B60FP610 t1,
      SQFNFDTA.BDRFP000 t2

WHERE t2.DQUTST = t1.TPOLST AND
       t2.DQUTNO = t1.TPOLNO AND
       t2.DQUTOC = t1.TPOLOC AND

       t2.DCVAPP = 'Y' AND

       t1.TELEYN = 'Y' AND
       t1.TELEVC != '' AND
       t1.TRTMET != '' AND
       t1.TPOLOC = 99

