

CREATE procedure [dbo].[usp_prod_list_composite_rates]  (  
    @RATE_ID int,  
    @HEALTH_PLAN_ID VARCHAR(4000) = NULL,  
    @DENTAL_PLAN_ID VARCHAR(4000) = NULL  )  
  
  
AS  
SET NOCOUNT ON;  
/*  
SMK20081014 Created proc, RETURN COMPOSITE RATES FOR A GIVEN RATE_ID  
SMK20081021 Modified, add call for dbo.usp_insert_original_sold_rates per Prasanna,Prem.This populated  
 total premium used by Quoting reports  
RAJ 03022009 MODIFIED FOR DE-COUPLING NEW PRODUCTS PER PRASANNA  
09/29/2009 RDIBBA   NOT CALLING ORGINAL SOLD RATE PROCEDURE  WHEN CASE STATUS IS ('S' OR XBS' OR 'F')  
   FOR TEXAS FLOW  TO FIX PRODUCTION ISSUE. as per req_id# 5809  
01/11/2010  Rdibba modified the logic to  return underwritten rates if exists as per req_id#6213  
01/31/2011  CKC  - added 'P' to rel_cd selects for 'S' (spouse) search for ckc131  
04/20/2011 CKC - added code for mcare 4 tier  search for -- ckcnmc  
08/08/2013 PB	- Added logic to calculate Taxes and fees.
*/  
BEGIN  
  
 --declare @HEALTH_PLAN_ID VARCHAR(4000)  
 --declare @rate_id int  
 --declare @dental_PLAN_ID VARCHAR(4000)  
 --SELECT @HEALTH_PLAN_ID = '74028,74032,74036,74040,74044,74012,74016,74020,74024,30081,30084,30087,30090,30093,30096,30099,30102,30105'  
 --SELECT @DENTAL_PLAN_ID = '72251,72238,72242,72245,72246,72243,72250,72237,72244,72252'  
 --SELECT @RATE_ID = 514364  
  
DECLARE    
  @eo_nbr int,@es_nbr int,@ec_e1_nbr int ,@ef_nbr int ,@user_id char(10),    
  @plan_type varchar(10),@prospect_id int ,@prop_num int ,@ERRMSG VARCHAR(100),    
  @CORP_ENT_CD VARCHAR(3),@EFF_DATE DATETIME, @TEFRA_IND CHAR(1), @FLAG CHAR(1),    
  @STEP_BAND_IND CHAR(1), @nonmcare_eo_cnt int,@chd_inc_cnt int,@sps_inc_cnt int,    
  @fly_inc_cnt int,@mcare_eo_cnt int, @mcare_dep_inc int, @mcare_spouse_cnt int,  -- ckcnmc    
  @mcare_child_cnt int , @U_CNT  int, @child_MC int         -- ckcnmc    
  ,@EO_Taxes_and_fees decimal(9,2) , @EC_E1_Taxes_and_fees decimal(9,2) , @ES_taxes_and_fees decimal(9,2) , @EF_taxes_and_fees decimal(9,2)  
    
  declare @separator_position int    
  declare @array_value varchar(1000)    
  declare @separator char(1)    
    
    
 CREATE TABLE #TMP_A    
 (PLAN_ID_H INT NULL)    
    
 CREATE TABLE #TMP_B    
 (PLAN_ID_D INT NULL)    
    
  SET @FLAG = 'N'    
    
  set @separator=','    
  set @HEALTH_PLAN_ID = @HEALTH_PLAN_ID+ @separator    
  while patindex('%' + @separator + '%' , @HEALTH_PLAN_ID) <> 0    
  begin    
   select @separator_position =  patindex('%' + @separator + '%' , @HEALTH_PLAN_ID)    
   select @array_value = left(@HEALTH_PLAN_ID, @separator_position - 1)    
   select @HEALTH_PLAN_ID= stuff(@HEALTH_PLAN_ID,1,@separator_position,'')    
   set @array_value=convert(int,@array_value)    
   insert into #TMP_A values (@array_value)    
  end    
    
  set @separator=','    
  set @DENTAL_PLAN_ID = @DENTAL_PLAN_ID+ @separator    
  while patindex('%' + @separator + '%' , @DENTAL_PLAN_ID) <> 0    
  begin    
   select @separator_position =  patindex('%' + @separator + '%' , @DENTAL_PLAN_ID)    
   select @array_value = left(@DENTAL_PLAN_ID, @separator_position - 1)    
   select @DENTAL_PLAN_ID= stuff(@DENTAL_PLAN_ID,1,@separator_position,'')    
   set @array_value=convert(int,@array_value)    
   insert into #TMP_B values (@array_value)    
  end    
    
    
 SELECT @CORP_ENT_CD = CORP_ENT_CD, @EFF_DATE = PROP_EDATE,    
 @TEFRA_IND = TEFRA_IND,@prospect_id = prospect_id,    
 @prop_num = prop_num    
 FROM PROS_PROP_OUT WITH (NOLOCK)    
 WHERE RATE_ID = @RATE_ID    
    
 SELECT @STEP_BAND_IND = STEP_BAND_IND    
 FROM PROPOSAL P with (nolock)    
 WHERE P.prospect_id = @prospect_id    
 AND P.prop_num  = @prop_num    
    
  --Get Single count-- -- DENTAL    
  select @eo_nbr = count(*)   FROM CENSUS_PROP_OUT  WITH (NOLOCK)    
        WHERE RATE_ID= @RATE_ID AND COVERAGE_TYPE_CD in ('CO', 'EO')    
    
 --Get employee plus spouse count--    
 select @es_nbr = count(*)   FROM CENSUS_PROP_OUT  WITH (NOLOCK)    
 WHERE RATE_ID= @RATE_ID AND COVERAGE_TYPE_CD in ('CS', 'ES')    
    
 --Get Single +1 count--    
 select @ec_e1_nbr = count(*)   FROM CENSUS_PROP_OUT  WITH (NOLOCK)    
 WHERE RATE_ID= @RATE_ID AND COVERAGE_TYPE_CD in ('CC', 'EC')    
    
 --Get Family count--    
 select @ef_nbr = count(*)   FROM CENSUS_PROP_OUT  WITH (NOLOCK)    
 WHERE RATE_ID= @RATE_ID AND COVERAGE_TYPE_CD in ('CF', 'EF')    
    
    
 IF EXISTS (SELECT 1    
 FROM SYSTEM_FACTOR WITH (NOLOCK)    
  WHERE FACTOR_ID = 'NMMEDICARE'    
  AND @EFF_DATE BETWEEN SYSTEM_EFF_DATE AND SYSTEM_EXP_DATE)    
  BEGIN    
  SET @FLAG = 'Y'    
  END    
    
    
  IF (@CORP_ENT_CD <> 'NM1' OR (@CORP_ENT_CD = 'NM1' AND @FLAG = 'N'))    
    
 BEGIN    
    
 select @user_id = isnull(modified_by,'system'), @plan_type = PLANTYPE_CD,    
  @prospect_id = prospect_id,@prop_num = prop_num    
 from pros_prop_out with (nolock)    
 where rate_id = @rate_id    
    
    
    
/* 09/29/2009 Rdibba   Not calling orginal sold rate procedure    
 When case status is ('S' OR XBS' OR 'F')   for Texas flow    
 To fix production issue. as per req_id#5809    
*/    
 Declare @STATUS char(5), @status_flag char(1)    
    
 set @status_flag ='N'    
    
 SELECT TOP 1 @STATUS = STATUS  FROM   dbo.NB_AUDIT A WITH(NOLOCK)    
        WHERE    PROSPECT_ID IN (SELECT  PROSPECT_ID    
              FROM dbo.PROS_PROP_OUT ppo with(nolock)    
       WHERE   RATE_ID = @RATE_ID    
       AND   ppo.CORP_ENT_CD = 'tx1')    
       ORDER BY  A.modified_ts DESC    
    
--select @STATUS as STATUS    
    
 IF  (@STATUS = 'S'  OR  @STATUS =  'XBS' OR @STATUS = 'F')    
  Begin    
  set @status_flag = 'Y'    
  end    
    
    
 IF @STATUS_FLAG = 'N'    
  BEGIN    
   exec dbo.usp_insert_original_sold_rates @prospect_id=@prospect_id,@prop_num=@prop_num,@Plantype=@plan_type,@userid=@user_id,@rate_id=@rate_id    
  END    
    
  --   exec dbo.usp_insert_original_sold_rates @prospect_id=@prospect_id,@prop_num=@prop_num,@Plantype=@plan_type,@userid=@user_id,@rate_id=@rate_id    
   --if (@@error <> 0)    
   --  begin    
   --   select @ERRMSG = 'Error executing usp_insert_original_sold_rates '    
   --    raiserror 20000 @ERRMSG    
   --    return -1    
   --  end    
    
    
--- 01/11/2010  Rdibba  modified the logic to  return underwritten rates if exists as per req_id#6213    
    
SELECT  @U_CNT = COUNT(*)   FROM  CVG_PREM_OUT WITH(NOLOCK)    
WHERE PROSPECT_ID = @PROSPECT_ID    
AND  PROP_NUM = @PROP_NUM    
AND  RATE_ID  = @RATE_ID    
AND  PREM_TYPE = 'U'    
    
IF (@U_CNT = 0)    
   SELECT  CPO.MED_PLAN_ID ,    
   CPO.EO_CVG_AMT,    
   CPO.EC_E1_CVG_AMT,    
   CPO.ES_CVG_AMT,    
   CPO.EF_CVG_AMT,    
   ((@eo_nbr* CPO.EO_CVG_AMT) +    
   (@ec_e1_nbr* CPO.EC_E1_CVG_AMT )  +    
   (@es_nbr* CPO.ES_CVG_AMT )  +    
   (@ef_nbr* CPO.EF_CVG_AMT )) AS TOTAL_CVG_AMT    
   --20130808 PB  
   ,ISNULL(((@eo_nbr* CPO.EO_Taxes_and_fees) +    
   (@ec_e1_nbr* CPO.EC_E1_Taxes_and_fees)  +    
   (@es_nbr* CPO.ES_Taxes_and_fees)  +    
   (@ef_nbr* CPO.EF_Taxes_and_fees)),'0.00') AS TOTAL_Taxes_and_fees    
   FROM  CVG_PREM_OUT CPO WITH(NOLOCK)    
   WHERE CPO.RATE_ID = @RATE_ID    
   ORDER BY  CPO.MED_PLAN_ID    
ELSE    
   SELECT  CPO.MED_PLAN_ID ,    
   CPO.EO_CVG_AMT,    
   CPO.EC_E1_CVG_AMT,    
   CPO.ES_CVG_AMT,    
   CPO.EF_CVG_AMT,    
   ((@eo_nbr* CPO.EO_CVG_AMT) +    
   (@ec_e1_nbr* CPO.EC_E1_CVG_AMT )  +    
   (@es_nbr* CPO.ES_CVG_AMT )  +    
   (@ef_nbr* CPO.EF_CVG_AMT )) AS TOTAL_CVG_AMT    
   --20130808 PB  
   ,((@eo_nbr* CPO.EO_Taxes_and_fees) +    
   (@ec_e1_nbr* CPO.EC_E1_Taxes_and_fees)  +    
   (@es_nbr* CPO.ES_Taxes_and_fees)  +    
   (@ef_nbr* CPO.EF_Taxes_and_fees)) AS TOTAL_Taxes_and_fees    
   FROM  CVG_PREM_OUT CPO WITH(NOLOCK)    
   WHERE CPO.RATE_ID = @RATE_ID    
 AND  CPO.PREM_TYPE ='U'   --- 01/11/2010  Rdibba modified the logic to  return underwritten rates if exists as per req_id#6213    
   ORDER BY  CPO.MED_PLAN_ID    
 END    
    
 ELSE    
    
 BEGIN    
   IF  (@TEFRA_IND ='Y' )    
    BEGIN    
    SELECT CPO.MED_PLAN_ID,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EO_CVG_AMT) AS VARCHAR(20)) AS EO_CVG_AMT,    
 CAST(CONVERT(DECIMAL(9,2),CPO.ES_CVG_AMT) AS VARCHAR(20)) AS ES_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EC_E1_CVG_AMT) AS VARCHAR(20)) AS EC_E1_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EF_CVG_AMT) AS VARCHAR(20))AS EF_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.MCARE_SINGLE_AMT) AS VARCHAR(20)) AS MCARE_SINGLE_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.MCARE_FAMILY_AMT) AS VARCHAR(20)) AS MCARE_FAMILY_AMT,    
 CAST(CONVERT(DECIMAL(9,2),isnull(CPO.MCARE_SPOUSE_AMT,0)) AS VARCHAR(20)) AS MCARE_SPOUSE_AMT, -- ckcnmc    
 CAST(CONVERT(DECIMAL(9,2),isnull(CPO.MCARE_CHILD_AMT,0)) AS VARCHAR(20)) AS MCARE_CHILD_AMT,  -- ckcnmc    
    CAST((CONVERT(DECIMAL(9,2),ROUND(CPO.EO_CVG_AMT*@eo_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(CPO.EC_E1_CVG_AMT*@ec_e1_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(CPO.ES_CVG_AMT*@es_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(CPO.EF_CVG_AMT*@ef_nbr,2))) AS VARCHAR(20)) AS TOTAL_CVG_AMT    
    --0    
    --20130808 PB  
      ,CAST((CONVERT(DECIMAL(9,2),ROUND(ISNULL(CPO.EO_Taxes_and_fees,'0.00')*@eo_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(ISNULL(CPO.EC_E1_Taxes_and_fees,'0.00')*@ec_e1_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(ISNULL(CPO.ES_Taxes_and_fees,'0.00')*@es_nbr,2))) +    
    (CONVERT(DECIMAL(9,2),ROUND(ISNULL(CPO.EF_Taxes_and_fees,'0.00')*@ef_nbr,2))) AS VARCHAR(20)) AS TOTAL_Taxes_and_fees   
    --  
    FROM DBO.CVG_PREM_OUT CPO  WITH (NOLOCK),    
  DBO.PROS_PROP_OUT PPO  WITH (NOLOCK)    
    WHERE ( CPO.PROSPECT_ID = PPO.PROSPECT_ID )    
  AND ( CPO.PROP_NUM = PPO.PROP_NUM )    
  AND ( CPO.RATE_ID = PPO.RATE_ID )    
  AND ( CPO.PREM_TYPE = 'Q'  )    
  AND ( PPO.PROSPECT_ID = @PROSPECT_ID )    
  AND ( PPO.PROP_NUM = @PROP_NUM )    
  AND ( PPO.RATE_ID = @RATE_ID )    
    ORDER BY  CPO.MED_PLAN_ID    
  END    
  ELSE    
  BEGIN    
      SELECT @nonmcare_eo_cnt = 0    
      SELECT @chd_inc_cnt = 0    
      SELECT @sps_inc_cnt = 0    
--      SELECT @fly_inc_cnt = 0    
      SELECT @mcare_eo_cnt = 0    
      SELECT @mcare_dep_inc = 0    
   SELECT @mcare_spouse_cnt = 0   -- ckcnmc    
   SELECT @mcare_child_cnt = 0   -- ckcnmc    
   SELECT @child_MC = 0     -- ckcnmc    
    
      CREATE TABLE #tmp_final_medicare (    
          rate_id int,    
          census_id int,    
          coverage_type_cd char(2),    
          mcare_pri_ind char(1),    
          mcare_sp_ind char(1),    
          mcare_ch_ind char(1),    
    mcare_ch_cnt int     -- ckcnmc    
       )    
    
      INSERT INTO #tmp_final_medicare    
  SELECT  rate_id, census_id, coverage_type_cd, mcare_pri_ind, 'N','N',0  -- ckcnmc    
  FROM dbo.census_prop_out WITH(NOLOCK)    
  WHERE rate_id = @RATE_ID    
  ORDER BY census_id    
    
     IF (@STEP_BAND_IND = 'B')  -- no dependent info entered    
      BEGIN    
        UPDATE #tmp_final_medicare    
   SET mcare_sp_ind = 'Y'    
        WHERE coverage_type_cd in ('ES','CS','EF','CF') and mcare_pri_ind = 'Y'    
    
        UPDATE #tmp_final_medicare    
   SET mcare_ch_ind = 'Y', mcare_ch_cnt = ( mcare_ch_cnt + 1 )    -- ckcnmc    
        WHERE coverage_type_cd in ('EC','CC','EF','CF') and mcare_pri_ind = 'Y'    
    END    
    ELSE    
     IF (@STEP_BAND_IND ='C')    -- This is dependent Info entered    
     BEGIN    
        UPDATE #tmp_final_medicare    
   SET mcare_sp_ind = case WHEN exists(Select mcare_pri_ind from census_dep_prop_out dpo    
              where dpo.rate_id = @RATE_ID and dpo.rel_cd in ('S','P')  -- ckc131    
                and dpo.mcare_pri_ind = 'Y' and #tmp_final_medicare.census_id = dpo.census_id)    
         then 'Y'    
         else 'N'    
        end,    
    mcare_ch_ind = case when exists(Select mcare_pri_ind from census_dep_prop_out dpo    
                                        where dpo.rate_id = @RATE_ID and dpo.rel_cd= 'C'    
                                        and dpo.mcare_pri_ind = 'Y'    
                                        and #tmp_final_medicare.census_id = dpo.census_id)    
         then 'Y'    
         else 'N'    
        end    
        FROM census_dep_prop_out dpo    
   WHERE #tmp_final_medicare.rate_id = dpo.rate_id    
    and #tmp_final_medicare.census_id = dpo.census_id    
    and #tmp_final_medicare.rate_id = @RATE_ID    
    
    
  create table #census_mc_child ( rate_id int, census_id int, all_mc int )  -- ckcnmc    
    
  insert into #census_mc_child             -- ckcnmc    
    select @rate_id, census_id, 1    
    from census_dep_prop_out    
    where rate_id = @rate_id and rel_cd = 'C' and mcare_pri_ind = 'Y'    
    
  update #census_mc_child               -- ckcnmc    
   set all_mc = 0    
  from #census_mc_child mc    
  inner join census_dep_prop_out cdpo on mc.rate_id = cdpo.rate_id    
    and mc.census_id = cdpo.census_id and cdpo.rel_cd = 'C' and mcare_pri_ind <> 'Y'    
    
  select @child_MC =  count(*) from #census_mc_child where all_mc = 1    -- ckcnmc    
    
  set @child_MC = isnull(@child_MC,0)            -- ckcnmc    
    
  update tfn                  -- ckcnmc    
   set tfn.mcare_ch_cnt = all_mc    
  from #tmp_final_medicare tfn    
   inner join #census_mc_child cmc on tfn.rate_id = cmc.rate_id    
      and tfn.census_id = cmc.census_id    
  where tfn.mcare_ch_ind = 'Y'    
    
  drop table #census_mc_child              -- ckcnmc    
     END    
    
  SELECT @nonmcare_eo_cnt = count(*) FROM #tmp_final_medicare WHERE mcare_pri_ind = 'N'    
    
     SELECT @mcare_eo_cnt = count(*) FROM #tmp_final_medicare WHERE mcare_pri_ind = 'Y'    
    
     SELECT @mcare_spouse_cnt = count(*) FROM #tmp_final_medicare WHERE    
           coverage_type_cd in ('ES','CS','EF','CF') and mcare_sp_ind = 'Y'    
    
     SELECT @sps_inc_cnt = count(*) FROM #tmp_final_medicare WHERE    
           coverage_type_cd in ('ES','CS','EF','CF') and mcare_sp_ind = 'N'    
    
     SELECT @chd_inc_cnt = count(*) FROM #tmp_final_medicare WHERE    
           coverage_type_cd in ('EC','CC','EF','CF') and mcare_ch_cnt = 0    
    
 SELECT @mcare_child_cnt = count(*) FROM #tmp_final_medicare WHERE    
           coverage_type_cd in ('EC','CC','EF','CF') and mcare_ch_cnt = 1    
    
 set @nonmcare_eo_cnt = isnull(@nonmcare_eo_cnt,0)    
 set @mcare_eo_cnt = isnull(@mcare_eo_cnt,0)    
 set @mcare_spouse_cnt = isnull(@mcare_spouse_cnt,0)    
 set @sps_inc_cnt = isnull(@sps_inc_cnt,0)    
 set @mcare_child_cnt = isnull(@mcare_child_cnt,0)    
 set @chd_inc_cnt = isnull(@chd_inc_cnt,0)    
    
--     SELECT @nonmcare_eo_cnt = @nonmcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EO','CO') and mcare_pri_ind = 'N'    
    
    
--     SELECT @mcare_eo_cnt = @mcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--          coverage_type_cd in ('EO','CO') and mcare_pri_ind = 'Y'    
    
     -- spouse combinations    
--     SELECT @nonmcare_eo_cnt = @nonmcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('ES','CS') and mcare_pri_ind = 'N' and mcare_sp_ind in ('N','Y')    
    
--     SELECT @sps_inc_cnt = @sps_inc_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('ES','CS') and mcare_pri_ind in ('N','Y') and mcare_sp_ind = 'N'    
    
--     SELECT @mcare_eo_cnt = @mcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('ES','CS') and mcare_pri_ind = 'Y' and mcare_sp_ind in ('N','Y')    
    
--     SELECT @mcare_spouse_cnt = @mcare_spouse_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('ES','CS') and mcare_pri_ind in ('N','Y') and mcare_sp_ind = 'Y'    
    
     -- child combinations    
--     SELECT @nonmcare_eo_cnt = @nonmcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EC','CC') and mcare_pri_ind = 'N' and mcare_ch_ind in ('N','Y')    
    
--     SELECT @chd_inc_cnt = @chd_inc_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EC','CC') and mcare_pri_ind in ('N','Y') and mcare_ch_ind = 'N'    
    
--     SELECT @mcare_eo_cnt = @mcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--   coverage_type_cd in ('EC','CC') and mcare_pri_ind = 'Y' and mcare_ch_ind in ('N','Y')    
    
-- add children to spouse    
-- ckcnmc     SELECT @mcare_dep_inc = @mcare_dep_inc + count(*) FROM #tmp_final_medicare WHERE    
-- ckcnmc           coverage_type_cd in ('EC','CC') and mcare_pri_ind in ('N','Y') and mcare_ch_ind = 'Y'    
    
-- SELECT @mcare_child_cnt = @mcare_child_cnt + @child_MC   -- ckcnmc    
    
     -- family combinations    
--     SELECT @nonmcare_eo_cnt = @nonmcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EF','CF') and mcare_pri_ind = 'N' and mcare_sp_ind in ('N','Y') and mcare_ch_ind in ('N','Y')    
    
--     SELECT @mcare_eo_cnt = @mcare_eo_cnt + count(*) FROM #tmp_final_medicare WHERE    
--       coverage_type_cd in ('EF','CF') and mcare_pri_ind = 'Y' and mcare_sp_ind in ('N','Y') and mcare_ch_ind in ('N','Y')    
    
--     SELECT @mcare_spouse_cnt = @mcare_spouse_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EF','CF') and mcare_pri_ind in ('N','Y') and mcare_sp_ind = 'Y' -- ckcnmc removed and mcare_ch_ind = 'N'    
    
--     SELECT @fly_inc_cnt = @fly_inc_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EF','CF') and mcare_pri_ind in ('N','Y') and mcare_sp_ind = 'N' -- ckcnmc removed and mcare_ch_ind = 'N'    
--    
--     SELECT @fly_inc_cnt = @fly_inc_cnt + count(*) FROM #tmp_final_medicare WHERE    
--           coverage_type_cd in ('EF','CF') and mcare_pri_ind in ('N','Y') and mcare_ch_ind = 'Y' and mcare_sp_ind = 'N'    
--    
--     SELECT @fly_inc_cnt = @fly_inc_cnt + count(*) FROM #tmp_final_medicare WHERE    
--          coverage_type_cd in ('EF','CF') and mcare_pri_ind in ('N','Y') and mcare_ch_ind = 'N' and mcare_sp_ind = 'Y'    
    
 set @mcare_spouse_cnt = isnull(@mcare_spouse_cnt,0)    
    
    SELECT CPO.MED_PLAN_ID,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EO_CVG_AMT) AS VARCHAR(20)) AS EO_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.ES_CVG_AMT) AS VARCHAR(20)) AS ES_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EC_E1_CVG_AMT) AS VARCHAR(20)) AS EC_E1_CVG_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EF_CVG_AMT) AS VARCHAR(20))AS EF_CVG_AMT,    
    --20130808 PB  
    CAST(CONVERT(DECIMAL(9,2),CPO.EO_Taxes_and_fees) AS VARCHAR(20)) AS EO_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.ES_Taxes_and_fees) AS VARCHAR(20)) AS ES_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EC_E1_Taxes_and_fees) AS VARCHAR(20)) AS EC_E1_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EF_Taxes_and_fees) AS VARCHAR(20))AS EF_Taxes_and_fees,   
    --  
    CAST(CONVERT(DECIMAL(9,2),CPO.MCARE_SINGLE_AMT) AS VARCHAR(20)) AS MCARE_SINGLE_AMT,    
    CAST(CONVERT(DECIMAL(9,2),CPO.MCARE_FAMILY_AMT) AS VARCHAR(20)) AS MCARE_FAMILY_AMT,    
    CAST(CONVERT(DECIMAL(9,2),isnull(CPO.MCARE_SPOUSE_AMT,0)) AS VARCHAR(20)) AS MCARE_SPOUSE_AMT, -- ckcnmc    
    CAST(CONVERT(DECIMAL(9,2),isnull(CPO.MCARE_CHILD_AMT,0)) AS VARCHAR(20)) AS MCARE_CHILD_AMT,  -- ckcnmc    
    cast((convert(decimal(9,2),round(cpo.EO_CVG_AMT * @nonmcare_eo_cnt,2))) +         -- ckcnmc    
  (convert(decimal(9,2),round(cpo.mcare_single_amt * @mcare_eo_cnt,2))) +    -- ckcnmc    
  (convert(decimal(9,2),round((cpo.ES_CVG_AMT - cpo.EO_CVG_AMT) * @sps_inc_cnt,2))) +    -- ckcnmc    
  (convert(decimal(9,2),round((isnull(cpo.MCARE_SPOUSE_AMT,0) - cpo.mcare_single_amt) * @mcare_spouse_cnt,2))) +       -- ckcnmc    
  (convert(decimal(9,2),round((cpo.EC_E1_CVG_AMT - cpo.EO_CVG_AMT) * @chd_inc_cnt,2))) +          -- ckcnmc    
  (convert(decimal(9,2),round((isnull(cpo.MCARE_CHILD_AMT,0) - cpo.mcare_single_amt) * @mcare_child_cnt,2))) as varchar(20)) AS TOTAL_CVG_AMT  -- ckcnmc    
    
--    (convert(decimal(9,2),round((cpo.EF_CVG_AMT - cpo.EO_CVG_AMT) * @fly_inc_cnt,2))) +    
--    (convert(decimal(9,2),round(cpo.mcare_single_amt * @mcare_eo_cnt,2))) +    
-- (convert(decimal(9,2),round((cpo.mcare_spouse_amt - cpo.mcare_single_amt) * @mcare_spouse_cnt,2))) +    
-- (convert(decimal(9,2),round((cpo.mcare_child_amt - cpo.mcare_single_amt) * @mcare_child_cnt,2))) as varchar(20)) AS TOTAL_CVG_AMT    
-- ckcnmc    (convert(decimal(9,2),round((cpo.mcare_family_amt - cpo.mcare_single_amt)* @mcare_dep_inc,2))) as varchar(20)) AS TOTAL_CVG_AMT    
,'0.00' AS Total_Taxes_and_fees
    
    FROM dbo.CVG_PREM_OUT cpo  with (nolock),    
  dbo.PROS_PROP_OUT ppo  with (nolock),    
  #TMP_A A    
    WHERE    
    ( cpo.PROSPECT_ID = ppo.PROSPECT_ID )    
    AND ( cpo.PROP_NUM = ppo.PROP_NUM )    
    AND ( cpo.rate_id = ppo.rate_id )    
    AND ( cpo.PREM_TYPE = 'Q'  )    
    AND ( ppo.PROSPECT_ID = @prospect_id )    
    AND ( ppo.PROP_NUM = @prop_num )    
    AND ( ppo.rate_id = @rate_id )    
    AND CPO.MED_PLAN_ID = A.PLAN_ID_H    
    
    
    UNION    
    
    SELECT CPO.MED_PLAN_ID,    
  CAST(CONVERT(DECIMAL(9,2),CPO.EO_CVG_AMT) AS VARCHAR(20)) AS EO_CVG_AMT,    
  CAST(CONVERT(DECIMAL(9,2),CPO.ES_CVG_AMT) AS VARCHAR(20)) AS ES_CVG_AMT,    
  CAST(CONVERT(DECIMAL(9,2),CPO.EC_E1_CVG_AMT) AS VARCHAR(20)) AS EC_E1_CVG_AMT,    
  CAST(CONVERT(DECIMAL(9,2),CPO.EF_CVG_AMT) AS VARCHAR(20))AS EF_CVG_AMT,    
      --20130808 PB  
    CAST(CONVERT(DECIMAL(9,2),CPO.EO_Taxes_and_fees) AS VARCHAR(20)) AS EO_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.ES_Taxes_and_fees) AS VARCHAR(20)) AS ES_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EC_E1_Taxes_and_fees) AS VARCHAR(20)) AS EC_E1_Taxes_and_fees,    
    CAST(CONVERT(DECIMAL(9,2),CPO.EF_Taxes_and_fees) AS VARCHAR(20))AS EF_Taxes_and_fees,   
    --  
  NULL AS MCARE_SINGLE_AMT,    
  NULL AS MCARE_FAMILY_AMT,    
  NULL AS MCARE_SPOUSE_AMT,    
  NULL AS MCARE_CHILD_AMT,    
  CAST((CONVERT(DECIMAL(9,2),ROUND(CPO.EO_CVG_AMT * @eo_nbr,2))) +    
  (CONVERT(DECIMAL(9,2),ROUND(CPO.EC_E1_CVG_AMT * @ec_e1_nbr,2))) +    
  (CONVERT(DECIMAL(9,2),ROUND(CPO.ES_CVG_AMT * @es_nbr,2))) +    
  (CONVERT(DECIMAL(9,2),ROUND(CPO.EF_CVG_AMT * @ef_nbr,2))) AS VARCHAR(20)) AS TOTAL_CVG_AMT   
  ,'0.00' AS Total_Taxes_and_fees 
    FROM dbo.CVG_PREM_OUT cpo  with (nolock),    
  dbo.PROS_PROP_OUT ppo  with (nolock),    
  #TMP_B B    
    WHERE ( cpo.PROSPECT_ID = ppo.PROSPECT_ID )    
     AND ( cpo.PROP_NUM = ppo.PROP_NUM )    
  AND ( cpo.rate_id = ppo.rate_id )    
     AND ( cpo.PREM_TYPE = 'Q'  )    
  AND ( ppo.PROSPECT_ID = @prospect_id )    
     AND ( ppo.PROP_NUM = @prop_num )    
  AND ( ppo.rate_id = @rate_id )    
     AND CPO.MED_PLAN_ID = B.PLAN_ID_D    
    order by CPO.MED_PLAN_ID    
    
    DROP TABLE #tmp_final_medicare    
END    
    
--select @nonmcare_eo_cnt, @mcare_eo_cnt, @mcare_spouse_cnt, @sps_inc_cnt , @mcare_child_cnt , @chd_inc_cnt    
    
END    
  DROP TABLE #tmp_A    
  DROP TABLE #tmp_B    
  
END  

