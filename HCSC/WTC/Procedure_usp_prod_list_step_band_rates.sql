

CREATE procedure [dbo].[usp_prod_list_step_band_rates]
@RATE_ID INT


AS
SET NOCOUNT ON;
/************************************************************************************************************************
Change Log:
SMK 20081014 Create proc to return step band rates for the given rate_id for NM
SMK 20090114 Accommodate for OK as well
RAJ 20090303 PRODUCTS SEPARATION PER PRASANNA
CKC 2011-01-31 Domestic Partner search for ckc131
RAJ 2012-03-02 REQ 10196
RS 2012-10-16 req 11053
RR 2012-10-17 req 11053 (see notes section)
*/
Begin

declare @TEFRA_IND char(1),@NM_MEDICARE_FACTOR datetime, @PROSPECT_ID INT, @eff_date DATETIME,
 @STEP_BAND_IND char(1),  @PROP_NUM int, @GROUPSIZE_NBR int


SELECT @eff_date = ppo.prop_edate , @PROSPECT_ID = PROSPECT_ID ,@PROP_NUM = PROP_NUM, @GROUPSIZE_NBR = GROUPSIZE_NBR
FROM dbo.pros_prop_out ppo with(nolock)
where ppo.rate_id = @rate_id

SELECT @STEP_BAND_IND =STEP_BAND_IND
FROM PROPOSAL P with (nolock)
WHERE P.prospect_id = @prospect_id
AND   P.prop_num = @prop_num


declare @tmp_census  TABLE (
    sex_cd char(1),
    med_plan_id int,
    cvg_amt decimal(9,2)
    )

declare @tmp_census_nm  TABLE (
    sex_cd char(1),
    med_plan_id int,
    cvg_amt decimal(9,2),
    mcare_cvg_amt decimal(9,2)
    )



declare  @tmp_census2   TABLE (
    plan_id int, premium decimal(9,2))

declare  @tmp_census2_nm   TABLE (
    plan_id int, premium decimal(9,2),mcare_premium decimal(9,2))


declare  @tmp_result   TABLE (
med_plan_id int, emp_prem_amt decimal(9,2),sps_prem_amt decimal(9,2) ,
child_prem_amt decimal (9,2))

declare  @tmp_result_nm   TABLE (
med_plan_id int, emp_prem_amt decimal(9,2),emp_mcare_prem_amt decimal(9,2), sps_prem_amt decimal(9,2) ,
sps_mcare_prem_amt decimal(9,2),child_prem_amt decimal (9,2))

/*1/14/2009*/
declare @CORP_ENT_CD CHAR(3)

 select @CORP_ENT_CD = ppo.CORP_ENT_CD
 from pros_prop_out ppo with(nolock)
 where ppo.rate_id = @rate_id

/*1/14/2009*/

  --Get the employee rates
 /* INSERT INTO @tmp_census
  SELECT cpo.sex_cd,  sbp.med_plan_id, sbp.male_cvg_amt, sbp.female_cvg_amt, sbp.child_cvg_amt
  FROM census_prop_out cpo with(nolock),  step_band_prem sbp with(nolock)
  WHERE cpo.rate_id = sbp.rate_id
    and cpo.age >= sbp.from_age
    and cpo.age <= sbp.to_age
    and sbp.rate_id = @rate_id
    and sbp.child_ind = 'N'
    and sbp.prem_type ='Q'
   ORDER BY sbp.med_plan_id      */


SELECT @NM_MEDICARE_FACTOR = system_eff_date
FROM SYSTEM_FACTOR SF WITH(NOLOCK)
WHERE FACTOR_ID = 'NMMedicare'

SELECT @TEFRA_IND =TEFRA_IND
FROM DBO.PROSPECT WITH(NOLOCK)
WHERE PROSPECT_ID =@PROSPECT_ID AND CORP_ENT_CD  ='NM1'



if (@CORP_ENT_CD ='NM1')
BEGIN

 if @TEFRA_IND = 'Y'

 begin
    INSERT INTO @tmp_census2_nm
     SELECT med_plan_id ,
			case when sex_cd = 'M' then sum(male_cvg_amt)
				 when sex_cd = 'F' then sum(female_cvg_amt) end ,
			mcare_premium = 0.0
     FROM /*#tmp_census      */
		census_prop_out cpo with(nolock),
		step_band_prem sbp with(nolock)
    WHERE cpo.rate_id = sbp.rate_id
		and cpo.age >= sbp.from_age
		and cpo.age <= sbp.to_age
		and sbp.child_ind = 'N'
		and sbp.prem_type ='Q'
		and sbp.rate_id = @rate_id
		and sbp.tefra_ind = 'N'
  GROUP BY med_plan_id, sex_cd

 end

 else

 begin
    INSERT INTO @tmp_census2_nm
     SELECT med_plan_id ,
		case when sex_cd = 'M' and mcare_pri_ind = 'N' then sum(male_cvg_amt)
			 when sex_cd = 'F'  and mcare_pri_ind = 'N' then sum(female_cvg_amt) end ,
		case when sex_cd = 'M' and mcare_pri_ind = 'Y' then sum(male_cvg_amt)
			 when sex_cd = 'F'  and mcare_pri_ind = 'Y' then sum(female_cvg_amt) end
     FROM /*#tmp_census      */
			census_prop_out cpo with(nolock),
			step_band_prem sbp with(nolock)
     WHERE cpo.rate_id = sbp.rate_id
		and cpo.age >= sbp.from_age
		and cpo.age <= sbp.to_age
		and sbp.child_ind = 'N'
		and sbp.prem_type = 'Q'
		and sbp.rate_id = @rate_id
		and isnull(cpo.mcare_pri_ind,'N') = isnull(sbp.tefra_ind ,'N')
     GROUP BY med_plan_id, sex_cd,mcare_pri_ind

 end



 /*select * from @tmp_census2*/

   INSERT INTO @tmp_result_nm
    SELECT  plan_id,  sum(premium), sum(mcare_premium),null,null,null
    FROM @tmp_census2_nm
    GROUP BY plan_id   order by plan_id

   delete from @tmp_census2_nm

/*  CREATE TABLE #spouse_temp
   (
    spouse_gender char(1),
    med_plan_id int,
    male_cvg_amt decimal(9,2),
    female_cvg_amt decimal(9,2),
    child_cvg_amt decimal(9,2),
    ben_plan_desc_shrt varchar(50),
    pharmacy varchar(20),
    plantype_cd varchar(1)
   )
  --get the spouse rates
  INSERT INTO #spouse_temp
   SELECT cpo.spouse_gender,
    sbp.med_plan_id, sbp.male_cvg_amt, sbp.female_cvg_amt, sbp.child_cvg_amt,
    bp.ben_plan_desc_shrt, bp.pharm_plan_cd, bp.plantype_cd
   FROM
    census_prop_out cpo with(nolock),
    step_band_prem sbp with(nolock),
    bnftplan bp with(nolock)
   WHERE
    cpo.rate_id = sbp.rate_id
    and cpo.spouse_age >= sbp.from_age
    and cpo.spouse_age <= sbp.to_age
    and bp.ben_plan_cd = sbp.med_plan_id
    and sbp.rate_id = @rate_id
    and cpo.prospect_id = @prospect_id
    and cpo.spouse_gender is not null
    and cpo.corp_ent_cd = @corp_ent_cd
    and sbp.child_ind = 'N'
    and bp.pharm_plan_cd = @pdp_plan_id
    --and bp.plantype_cd = @plan_type
    and cpo.coverage_type_cd not in ('LO','OH','DC','N')--06/10/2008 - RD
    and sbp.prem_type ='Q'
   ORDER BY sbp.med_plan_id


  CREATE TABLE #spouse_temp2
   ( premium decimal(9,2),
    ben_plan_desc_shrt varchar(50),
    med_plan_id int,
    pharmacy varchar(20),
    plantype_cd varchar(1)
   )      */

 if @tefra_ind = 'Y'

 begin

    INSERT INTO @tmp_census_nm
     SELECT spouse_gender, med_plan_id,
		case when spouse_gender = 'M' then sum(male_cvg_amt)
			 when spouse_gender = 'F' then sum(female_cvg_amt) end,
		mcare_cvg_amt = 0.0
     FROM census_prop_out cpo with(nolock), step_band_prem sbp with(nolock)
     WHERE cpo.rate_id = sbp.rate_id
      and cpo.spouse_age >= sbp.from_age
      and cpo.spouse_age <= sbp.to_age
      and cpo.spouse_gender is not null
      and cpo.coverage_type_cd not in ('LO','OH','DC','N')
  and sbp.tefra_ind = 'N'
      and sbp.child_ind = 'N'
      and sbp.prem_type ='Q'
      and sbp.rate_id = @rate_id
     GROUP BY med_plan_id, spouse_gender

     INSERT INTO @tmp_census2_nm select med_plan_id ,isnull(sum(cvg_amt),0),mcare_cvg_amt
  from @tmp_census_nm GROUP BY med_plan_id,mcare_cvg_amt

 end

 else

 begin

  IF  @STEP_BAND_IND = 'B'
  begin
   INSERT INTO @tmp_census_nm
      SELECT spouse_gender, med_plan_id,
		case when spouse_gender = 'M' and mcare_pri_ind = 'N' then sum(male_cvg_amt)
			 when spouse_gender = 'F' and mcare_pri_ind = 'N' then sum(female_cvg_amt) end,
		case when spouse_gender = 'M' and mcare_pri_ind = 'Y' then sum(male_cvg_amt)
			 when spouse_gender = 'F' and mcare_pri_ind = 'Y' then sum(female_cvg_amt) end
      FROM census_prop_out cpo with(nolock), step_band_prem sbp with(nolock)
      WHERE cpo.rate_id = sbp.rate_id
		and cpo.spouse_age >= sbp.from_age
		and cpo.spouse_age <= sbp.to_age
		and cpo.spouse_gender is not null
		and cpo.coverage_type_cd not in ('LO','OH','DC','N')
		and sbp.child_ind = 'N'
		and sbp.prem_type ='Q'
		and sbp.rate_id = @rate_id
		and isnull(cpo.mcare_pri_ind,'N') = isnull(sbp.tefra_ind ,'N')
      GROUP BY med_plan_id, spouse_gender,mcare_pri_ind

   INSERT INTO @tmp_census2_nm select med_plan_id ,isnull(sum(cvg_amt),0) , isnull(sum(mcare_cvg_amt),0)
   from @tmp_census_nm GROUP BY med_plan_id
  end
  else
  begin
   INSERT INTO @tmp_census_nm
    SELECT spouse_gender, med_plan_id,
		case when spouse_gender = 'M' and cdpo.mcare_pri_ind = 'N' then sum(male_cvg_amt)
			 when spouse_gender = 'F' and cdpo.mcare_pri_ind = 'N' then sum(female_cvg_amt) end,
		case when spouse_gender = 'M' and cdpo.mcare_pri_ind = 'Y' then sum(male_cvg_amt)
			 when spouse_gender = 'F' and cdpo.mcare_pri_ind = 'Y' then sum(female_cvg_amt) end
      FROM census_prop_out cpo with(nolock),
			step_band_prem sbp with(nolock),
			census_dep_prop_out cdpo with(nolock)
      WHERE cpo.rate_id = sbp.rate_id
		and cpo.spouse_age >= sbp.from_age
		and cpo.spouse_age <= sbp.to_age
		and cpo.spouse_gender is not null
		and cpo.spouse_gender in ('M','F')
		and cpo.coverage_type_cd not in ('LO','OH','DC','N')
		and sbp.child_ind = 'N'
		and sbp.prem_type = 'Q'
		and sbp.rate_id = @rate_id
		and isnull(cdpo.mcare_pri_ind,'N') = isnull(sbp.tefra_ind ,'N')
		and cdpo.rate_id = cpo.rate_id
		and cdpo.rel_cd in ('S','P')			-- ckc131
		and cpo.census_id = cdpo.census_id
      GROUP BY med_plan_id, spouse_gender,cdpo.mcare_pri_ind

      INSERT INTO @tmp_census2_nm select med_plan_id ,isnull(sum(cvg_amt),0), isnull(sum(mcare_cvg_amt),0)
   from @tmp_census_nm GROUP BY med_plan_id
  end




 end

/*  CREATE TABLE #tmp_sps_prem
   ( spouse_premium decimal(9,2),
    ben_plan_desc_shrt varchar(50),
    med_plan_id int,
    pharmacy varchar(20),
    plantype_cd varchar(1)
   )     */

 update  @tmp_result_nm
 set sps_prem_amt = isnull(premium,0),
  sps_mcare_prem_amt = isnull(mcare_premium,0)
  from  @tmp_census2_nm
  where  med_plan_id =  plan_id

  delete from @tmp_census2_nm


/*  CREATE TABLE #child_temp
   (
    num_of_children int,
    med_plan_id int,
     from_age int,
     to_age int,
    child_cvg_amt decimal(9,2),
    ben_plan_desc_shrt varchar(50),
    pharmacy varchar(20),
    plantype_cd varchar(1)
   )

  --get dependent rates
  INSERT INTO #child_temp
   SELECT cpo.num_of_children,
    sbp.med_plan_id,
    sbp.from_age,
    sbp.to_age,
    sbp.child_cvg_amt,
    bp.ben_plan_desc_shrt,
    bp.pharm_plan_cd,
    bp.plantype_cd
   FROM
    census_prop_out cpo with(nolock),
    step_band_prem sbp with(nolock),
    bnftplan bp with(nolock)
   WHERE  cpo.rate_id = sbp.rate_id
    and bp.ben_plan_cd = sbp.med_plan_id
    and sbp.rate_id = @rate_id
    and cpo.prospect_id = @prospect_id
    and cpo.num_of_children is not null
    and sbp.child_ind = 'Y'
    and cpo.corp_ent_cd = @corp_ent_cd
    and bp.pharm_plan_cd = @pdp_plan_id
    --and bp.plantype_cd = @plan_type
    and sbp.prem_type ='Q'
    and cpo.coverage_type_cd not in ('LO','OH','DC','N') --06/10/2008 - RD
   ORDER BY sbp.med_plan_id


  CREATE TABLE #tmp_child_prem
   (
    child_premium decimal(9,2),
    ben_plan_desc_shrt varchar(50),
    med_plan_id int,
    pharmacy varchar(20),
    plantype_cd varchar(1)
   )      */

   INSERT INTO @tmp_census2_nm
    SELECT  med_plan_id , sum(isnull(child_cvg_amt,0)),0.0
    FROM census_prop_out cpo with(nolock),
		step_band_prem sbp with(nolock)
    WHERE  cpo.rate_id = sbp.rate_id
		and cpo.num_of_children is not null
		and cpo.coverage_type_cd not in ('LO','OH','DC','N') --06/10/2008 - RD
		and sbp.rate_id = @rate_id
		and sbp.child_ind = 'Y'
		and sbp.prem_type = 'Q'
		and num_of_children between from_age and to_age
    GROUP BY med_plan_id

  update  @tmp_result_nm
		set child_prem_amt =  isnull(premium,0)
  from  @tmp_census2_nm
  where  med_plan_id =  plan_id


 if @eff_date >= @NM_MEDICARE_FACTOR

 begin
   SELECT
		med_plan_id,isnull(emp_prem_amt,0) as emp_prem_amt ,
		isnull(emp_mcare_prem_amt,0) as emp_mcare_prem_amt,
		dep_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0)),
		isnull(sps_mcare_prem_amt,0) as sps_mcare_prem_amt,
		total_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0) + isnull(emp_prem_amt,0)
		 + isnull(emp_mcare_prem_amt,0) + isnull(sps_mcare_prem_amt,0) )
   FROM @tmp_result_nm
   ORDER BY med_plan_id
 end
 else
 begin
   SELECT
		med_plan_id,emp_prem_amt ,  dep_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0)),
		total_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0) + isnull(emp_prem_amt,0) )
   FROM @tmp_result_nm
   ORDER BY med_plan_id
 end

 END /*NM1*/

/*1/14/2009 */
IF (@CORP_ENT_CD = 'OK1')
BEGIN
   INSERT INTO @tmp_census2
    SELECT med_plan_id ,
    case when sex_cd = 'M' then sum(male_cvg_amt) when sex_cd = 'F' then sum(female_cvg_amt) end
    FROM    census_prop_out cpo with(nolock),  step_band_prem sbp with(nolock)
     WHERE cpo.rate_id = sbp.rate_id
 and cpo.age >= sbp.from_age
 and cpo.age <= sbp.to_age
 AND CPO.COVERAGE_TYPE_CD NOT IN ('LO','OH','DC')
     and sbp.child_ind = 'N'
 and sbp.prem_type ='Q'
 AND SBP.REL_CD = 'E'
 and sbp.rate_id = @rate_id
    GROUP BY med_plan_id, sex_cd
    
    
 ------------------------------      INSERT INTO @tmp_result
 ------------------------------   SELECT  plan_id,  sum(premium), null,null
 ------------------------------   FROM @tmp_census2
	-------------------------------- RR 03/02/2012 REQ 10196 added the where clause
	------------------------------where plan_id not in (select distinct pln.pln_id  
 ------------------------------       from BUSACQSN_PLN pln,BUSACQSN_PLN_GRP_XREF xref, BUSACQSN_PLN_GRP pln_grp  
 ------------------------------       where pln.pln_id = xref.pln_id  
 ------------------------------       and xref.pln_grp_id = pln_grp.pln_grp_id  
 ------------------------------       and pln.pln_eff_dt = (select system_eff_date  
 ------------------------------             From system_factor  
 ------------------------------             where factor_id = 'OKHSADt')  
 ------------------------------       and pln.corp_ent_cd = 'OK1'  
 ------------------------------       and pln_grp.pln_grp_cd not in(SELECT pln_grp_cd FROM dbo.PLN_GRP_CD WITH (NOLOCK) WHERE CORP_ENT_CD ='OK1' AND PLN_CONTEXT ='INC'AND @eff_date between PLN_ST_DT and PLN_END_DT))--'BCTS','BCTD','BOTS'))  
 ------------------------------   GROUP BY plan_id  
 ------------------------------order by plan_id
  --RS 10/16/2012 req 11053 modified where clause 

  --RR 10/17/2012 req 11053 changed above where clause to below where clause

   INSERT INTO @tmp_result
    SELECT  plan_id,  sum(premium), null,null
    FROM @tmp_census2
	-- RR 03/02/2012 REQ 10196 added the where clause
	where plan_id in (select distinct pln.pln_id  
        from BUSACQSN_PLN pln,BUSACQSN_PLN_GRP_XREF xref, BUSACQSN_PLN_GRP pln_grp  
        where pln.pln_id = xref.pln_id  
        and xref.pln_grp_id = pln_grp.pln_grp_id 
        and @GROUPSIZE_NBR between isnull(GRPSZ_MIN_NBR,0) and isnull(GRPSZ_MAX_NBR,9999)   
        and pln.corp_ent_cd = 'OK1'  
        and pln_grp.pln_grp_cd in(SELECT pln_grp_cd 
											FROM dbo.PLN_GRP_CD WITH (NOLOCK) 
											WHERE CORP_ENT_CD ='OK1' 
											AND PLN_CONTEXT ='INC'
											AND @eff_date between PLN_ST_DT and PLN_END_DT))--'BCTS','BCTD','BOTS'))  
    GROUP BY plan_id  
 order by plan_id  





   delete from @tmp_census2

 /*Premiums for Spouse */

   INSERT INTO @tmp_census
    SELECT spouse_gender, med_plan_id, case when spouse_gender = 'M' then sum(male_cvg_amt) when spouse_gender = 'F' then sum(female_cvg_amt) end
    FROM     census_prop_out cpo with(nolock),   step_band_prem sbp with(nolock)
    WHERE     cpo.rate_id = sbp.rate_id
     and cpo.spouse_age >= sbp.from_age
     and cpo.spouse_age <= sbp.to_age
     and cpo.spouse_gender is not null
     and cpo.coverage_type_cd not in ('LO','OH','DC')
     and sbp.child_ind = 'N'
     and sbp.prem_type ='Q'
	 and sbp.rel_cd in ('S','P')    -- ckc131
     and sbp.rate_id = @rate_id
    GROUP BY med_plan_id, spouse_gender

    INSERT INTO @tmp_census2 select med_plan_id ,isnull(sum(cvg_amt),0) from @tmp_census GROUP BY med_plan_id

 update  @tmp_result set sps_prem_amt = isnull(premium,0)
  from  @tmp_census2
  where  med_plan_id =  plan_id

  delete from @tmp_census2


 /*Premiums for Children*/
   INSERT INTO @tmp_census2
    SELECT  med_plan_id , sum(isnull(child_cvg_amt,0))
       FROM census_prop_out cpo with(nolock),
     step_band_prem sbp with(nolock)
    WHERE  cpo.rate_id = sbp.rate_id
    and cpo.num_of_children is not null
     and cpo.coverage_type_cd not in ('LO','OH','DC')
     and sbp.rate_id = @rate_id
     and sbp.child_ind = 'Y'
     and sbp.prem_type ='Q'
     and num_of_children between from_age and to_age
    GROUP BY med_plan_id

  update  @tmp_result set child_prem_amt =  isnull(premium,0)
  from  @tmp_census2
  where  med_plan_id =  plan_id


  SELECT
    med_plan_id, emp_prem_amt, dep_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0)),
    total_prem_amt = (isnull(sps_prem_amt,0) + isnull(child_prem_amt,0) + isnull(emp_prem_amt,0) )
   FROM @tmp_result
   ORDER BY med_plan_id

END    /*1/14/2009 */

end


