CREATE  procedure [dbo].[usp_insert_original_sold_rates]
	 		@prospect_id int,
			@prop_num int,
			@Plantype varchar(10),
			@Userid varchar(10),
			@rate_id int
as
SET NOCOUNT ON;
begin
/**************************************
 * Variable Declarations
UPDATE LOG:
smtz20050912 update cpo.plan_opt with correct ben_plan_nbr, but without bnftplan_xref since all plans are rated after 1/1/06
smtz20050921 plan_opt is used for health only subscriber info
usp_insert_original_sold_rates
ST 11-08-2005 Sending Med Plan ID instead of plan name to usp_insert_quoted_or_sold_rates to avoid ambiguity
BB 03-19-2007 Updating to allow for the new texas triple option in NB
	search for: -- BDB 03192007
BB 04-05-2007 Updating to get the proper census counts to be used for the updating of the total premium
	search for: -- BDB 04052007
BB 05-18-2007 Updated to get the stage 2 data if the case was brought in to enrollment from prelims
	search for: -- BDB20070518
2007-07-23 CDZ - added join (pros_prop_out) to get corp_ent_cd
SMK 06/09/2008 - For Core2-2008, Updated to use the NBOC_PROSPECT_ID ,NBOC_PROP_NUM , NBOC_RATE_ID if a NBOC_PROSPECT_ID is passed in as a parm
  Search For : --SMK 060908
SMK 09/09/08 - NBOC REQ ,GLYCO,VIJAYN ,UPDATE NBOC_MOD_HIST.SKP_RTG_IND ='N'
 Search For : --SMK 090908
* 08/25/2009 : RDIBBA  Modified the input parameter usp_delete_quoted_or_sold_rates for tripleoption req_id#5507
 **************************************/

DECLARE  @plan_id int
	,@eo decimal(9,2)
	,@ec decimal(9,2)
	,@es decimal(9,2)
	,@ef decimal(9,2)
	,@eo_num int
	,@ec_num int
	,@es_num int
	,@ef_num int
	,@eo_num1 int
	,@ec_num1 int
	,@es_num1 int
	,@ef_num1 int
	,@total decimal(9,2)
	,@ACCT_ENRL_NBR int
	,@ben_plan_cd int
	,@iplanCD int
	,@ipdpCD int
	,@acct_eff_dt datetime
	,@Plantype_wk char(1)
	,@iHmoPlanCd int
	,@quote_rate_id int
	,@quote_prospect_id int
	,@quote_prop_num int
	,@Plantype_o char(1)
	,@nb_ind int
	,@hlth_cvg_typ   char(1)
	,@coverage_type  char(1)
	,@sgrs_plan_num1 int
	,@ben_agmt_nbr1  int
	,@sgrs_plan_num2 int
	,@ben_agmt_nbr2  int
	,@MOP_Ind int
	,@date_factor datetime
	,@new_plan_type char(1)
	,@carrier_cd char(1) --CKM20041112 added for insert into uw_prem_out
	,@carrier_plan_cd integer --CKM20041112 added for insert into uw_prem_out
	,@system_eff_date datetime, @system_exp_date datetime, @RateAllPlans varchar(1)  --smtz20050912 get effective date for Rating All Plans in TX
	,@hp_ind char(1)
	,@NBOC_IND CHAR(1) --SMK 060908

DECLARE @TMP_CENSUS TABLE (census_id INT, sgrs_plan_num INT )  --SMK 060908


--CKM20041022 initialize the effective data
select @acct_eff_dt = prop_edate from pros_prop_out with (nolock) where rate_id=@rate_id --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
--CKM20041022 initialize @nb_ind to 0 (quoting)
select @nb_ind = 0
if @prospect_id in (select nb_prospect_id from case_xref)
   begin
	select @nb_ind = 1
   end

--SMK 060908
if @prospect_id in (select nboc_prospect_id from case_xref)
	select @nb_ind = 1   ,@nboc_ind ='Y'
ELSE
	select @nboc_ind ='N'


if @Plantype = '1'
begin
exec usp_get_actual_plan @prospect_id , @prop_num, @Plantype, @new_plan_type output
select @Plantype = @new_plan_type
end

--CKM20041025 set @MOP_Ind to 1 if this is a MOP
select @MOP_Ind = 0
if @Plantype = 'M'
   begin
	select @MOP_Ind = 1
   end

/*CKM20042025 - Get the date when mops plans combination will be stored in bnftplan_xref*/
select 	@date_factor =date_factor
from 	system_factor with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
where 	factor_id = 'mopcombo'
and 	@acct_eff_dt between system_eff_date and system_exp_date

--smtz20050912 update cpo.plan_opt with correct ben_plan_nbr, but without bnftplan_xref since all plans are rated after 1/1/06
select @system_eff_date = system_eff_date, @system_exp_date = system_exp_date from system_factor with(nolock) where factor_id = 'RateAlPlan'
select @RateAllPlans = case when ppo.prop_edate between @system_eff_date and @system_exp_date then 'Y' else 'N' end from pros_prop_out ppo with(nolock) where rate_id = @rate_id
	IF @RateAllPlans = 'Y'
	BEGIN

	--SMK 060908
	IF (@NBOC_IND ='Y')
		INSERT INTO @TMP_CENSUS SELECT  cpo.census_id, eba.sgrs_plan_num
		from 	census_prop_out cpo with(nolock),
			enrl_mem em with(nolock),
			enrl_ben_agmt eba with(nolock),
			enrl_mem_ben emb with(nolock),
			case_xref cx with(nolock)
		where
			cx.nboc_prospect_id = @prospect_id
			and cx.nboc_prop_num = @prop_num
			and em.acct_enrl_nbr = cx.acct_enrl_nbr
			and emb.acct_enrl_nbr = em.acct_enrl_nbr
			and emb.sub_seq_nbr = em.sub_seq_nbr
			and emb.mem_nbr = em.mem_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and eba.ben_agmt_nbr = emb.ben_agmt_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and emb.ben_agmt_nbr = eba.ben_agmt_nbr
			and eba.acct_enrl_nbr = cx.acct_enrl_nbr
			and cpo.prospect_id = cx.nboc_prospect_id
			and cpo.prop_num = cx.nboc_prop_num
			and cpo.census_id = em.sub_seq_nbr
			and emb.ins_typ_cd = 'H'	--smtz20050921 plan_opt is used for health only subscriber info
			and em.rel_cd = 'SUB'		--smtz20050921 plan_opt is used for health only subscriber info

		ELSE
		--smk060908select cpo.census_id, eba.sgrs_plan_num into #tmp_census
		INSERT INTO @TMP_CENSUS SELECT  cpo.census_id, eba.sgrs_plan_num
		from 	census_prop_out cpo with(nolock),
			enrl_mem em with(nolock),
			enrl_ben_agmt eba with(nolock),
			enrl_mem_ben emb with(nolock),
			case_xref cx with(nolock)
		where
			cx.nb_prospect_id = @prospect_id
			and cx.nb_prop_num = @prop_num
			and em.acct_enrl_nbr = cx.acct_enrl_nbr
			and emb.acct_enrl_nbr = em.acct_enrl_nbr
			and emb.sub_seq_nbr = em.sub_seq_nbr
			and emb.mem_nbr = em.mem_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and eba.ben_agmt_nbr = emb.ben_agmt_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and emb.ben_agmt_nbr = eba.ben_agmt_nbr
			and eba.acct_enrl_nbr = cx.acct_enrl_nbr
			and cpo.prospect_id = cx.nb_prospect_id
			and cpo.prop_num = cx.nb_prop_num
			and cpo.census_id = em.sub_seq_nbr
			and emb.ins_typ_cd = 'H'	--smtz20050921 plan_opt is used for health only subscriber info
			and em.rel_cd = 'SUB'		--smtz20050921 plan_opt is used for health only subscriber info


		update census_prop_out
			set plan_opt = tc.sgrs_plan_num
			from @TMP_CENSUS TC ,
				--SMK060908 #tmp_census tc,
				census_prop_out cpo with(nolock)
			where	tc.census_id = cpo.census_id
				and cpo.prospect_id = @prospect_id
				and cpo.prop_num = @prop_num

			--SMK060908drop table #tmp_census

	END
	ELSE
	BEGIN
		/*CKM20041027 - update plan_opt in census_prop_out*/
	--SMK 060908
	IF (@NBOC_IND ='Y')
		update cpo
		set cpo.plan_opt = eba.SGRS_PLAN_NUM
		from	census_prop_out cpo,
			bnftplan_xref bx with (nolock),
			enrl_mem em with (nolock),
			enrl_mem_ben emb with (nolock),
			enrl_ben_agmt eba with (nolock),
			case_xref cx with (nolock)
		where	cx.nboc_prospect_id = @prospect_id
			and cx.nboc_prop_num = @prop_num
			and em.acct_enrl_nbr = cx.acct_enrl_nbr
			and emb.acct_enrl_nbr = em.acct_enrl_nbr
			and emb.sub_seq_nbr = em.sub_seq_nbr
			and emb.mem_nbr = em.mem_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and eba.ben_agmt_nbr = emb.ben_agmt_nbr
			and bx.ben_plan_cd = eba.sgrs_plan_num
			and cpo.prospect_id = cx.nboc_prospect_id
			and cpo.prop_num = cx.nboc_prop_num
			and cpo.census_id = em.sub_seq_nbr
			and ((bx.eff_date  <= @acct_eff_dt) and (@acct_eff_dt <= bx.exp_date))

	ELSE
		update cpo
		set cpo.plan_opt = eba.SGRS_PLAN_NUM
		from	census_prop_out cpo,
			bnftplan_xref bx with (nolock),
			enrl_mem em with (nolock),
			enrl_mem_ben emb with (nolock),
			enrl_ben_agmt eba with (nolock),
			case_xref cx with (nolock)
		where	cx.nb_prospect_id = @prospect_id
			and cx.nb_prop_num = @prop_num
			and em.acct_enrl_nbr = cx.acct_enrl_nbr
			and emb.acct_enrl_nbr = em.acct_enrl_nbr
			and emb.sub_seq_nbr = em.sub_seq_nbr
			and emb.mem_nbr = em.mem_nbr
			and eba.acct_enrl_nbr = emb.acct_enrl_nbr
			and eba.ben_agmt_nbr = emb.ben_agmt_nbr
			and bx.ben_plan_cd = eba.sgrs_plan_num
			and cpo.prospect_id = cx.nb_prospect_id
			and cpo.prop_num = cx.nb_prop_num
			and cpo.census_id = em.sub_seq_nbr
			and ((bx.eff_date  <= @acct_eff_dt) and (@acct_eff_dt <= bx.exp_date))
	END


/***************************************************************************
CKM20041022 - Get oe, es, ec, ef counts to be used for HMO total calculation
****************************************************************************/

/*** HMO counts ***/
	--CM20040527 Add MOP indicator to check eligibility for HMO side of MOP
	set @hlth_cvg_typ = 'H'
	set @coverage_type = 'O'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type,@Plantype, @nb_ind, @hlth_cvg_typ, @MOP_Ind, @cnt = @eo_num output

	set @coverage_type = 'C'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type,@Plantype, @nb_ind, @hlth_cvg_typ, @MOP_Ind, @cnt = @ec_num output

	set @coverage_type = 'S'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type,@Plantype, @nb_ind, @hlth_cvg_typ, @MOP_Ind, @cnt = @es_num output

	set @coverage_type = 'F'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type,@Plantype, @nb_ind, @hlth_cvg_typ, @MOP_Ind, @cnt = @ef_num output


--Now that we have the counts need to update cvg_prem_out totals for HMO
	update cvg_prem_out
	set    total_premium_amt = ((EO_CVG_AMT    * @eo_num)
				   +(EC_E1_CVG_AMT * @ec_num)
				   +(ES_CVG_AMT    * @es_num)
				   +(EF_CVG_AMT    * @ef_num))
	where  rate_id = @rate_id
	and    PLANTYPE_CD in ('I','Y')
	and    prem_type   = 'Q'

/**************************************************************************************
CKM20041022 - End of HMO total calculation
***************************************************************************************/

/**************************************************************************************
CKM20041022 - Getting counts for PPO side for Total calculation
***************************************************************************************/
if @Plantype = 'X' and @nb_ind = 1 --get counts for new business Dual PPO
  begin
	--SMK 060908
	IF (@NBOC_IND ='Y')
		select @acct_enrl_nbr = acct_enrl_nbr from case_xref with (nolock) --rr 07/29/2005, added with (nolock) to avoid locking issue per rsd
		where nboc_prospect_id = @prospect_id and nboc_prop_num = @prop_num
	ELSE
		select @acct_enrl_nbr = acct_enrl_nbr from case_xref with (nolock)
		where nb_prospect_id = @prospect_id and nb_prop_num = @prop_num

	select top 1
		@sgrs_plan_num1 = eb.sgrs_plan_num,
		@ben_agmt_nbr1 = eb.BEN_AGMT_NBR
	from 	enrl_ben_agmt eb with (nolock), 		bnftplan b with (nolock)
	where 	eb.sgrs_plan_num = b.ben_plan_cd
	and 	eb.acct_enrl_nbr = @acct_enrl_nbr
	and	b.plantype_cd <> 'Q' --Exclude dental
 	order by b.ben_plan_cd asc

	select top 1
		@sgrs_plan_num2 = eb.sgrs_plan_num,
		@ben_agmt_nbr2 = eb.BEN_AGMT_NBR
	from 	enrl_ben_agmt eb with (nolock),
		bnftplan b with (nolock)
	where 	eb.sgrs_plan_num = b.ben_plan_cd
	and 	eb.acct_enrl_nbr = @acct_enrl_nbr
	and	b.plantype_cd <> 'Q' --Exclude dental
	order by b.ben_plan_cd desc

	--reset count variables to zero!
	select 	@eo_num = 0,
		@ec_num = 0,
		@es_num = 0,
		@ef_num = 0
	/*** Dual PPO counts - first plan ***/
	set @coverage_type = 'O'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
	 @cnt = @eo_num output

	set @coverage_type = 'C'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
	 @cnt = @ec_num output

	set @coverage_type = 'S'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
	 @cnt = @es_num output

	set @coverage_type = 'F'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
	 @cnt = @ef_num output

	/*** Dual PPO counts - second plan ***/
	set @coverage_type = 'O'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr2,
	 @cnt = @eo_num1 output

	set @coverage_type = 'C'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr2,
	 @cnt = @ec_num1 output

	set @coverage_type = 'S'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr2,
	 @cnt = @es_num1 output

	set @coverage_type = 'F'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr2,
	 @cnt = @ef_num1 output

	--Now we have the counts needed to update cvg_prem_out totals for the first plan for dual PPO
	update cvg_prem_out
	set    total_premium_amt = ((EO_CVG_AMT    * @eo_num)
				   +(EC_E1_CVG_AMT * @ec_num)
				   +(ES_CVG_AMT    * @es_num)
				   +(EF_CVG_AMT    * @ef_num))
	where  rate_id = @rate_id
	and    MED_PLAN_ID = @sgrs_plan_num1
	and    prem_type   = 'Q'

	--CKM20041022 Now we have the counts needed to update cvg_prem_out totals for the second plan for dual PPO
	update cvg_prem_out
	set    total_premium_amt = ((EO_CVG_AMT    * @eo_num1)
				   +(EC_E1_CVG_AMT * @ec_num1)
				   +(ES_CVG_AMT    * @es_num1)
				   +(EF_CVG_AMT    * @ef_num1))
	where  rate_id = @rate_id
	and    MED_PLAN_ID = @sgrs_plan_num2
	and    prem_type   = 'Q'

   end
/*CKM20041022 end new business dual PPO section */
else
if @Plantype in ('N', 'O') and @nb_ind = 1 -- BDB 04052007 Start of changes
  begin
	--SMK 060908
	IF (@NBOC_IND ='Y')
		select @acct_enrl_nbr = acct_enrl_nbr from case_xref with (nolock) --rr 07/29/2005, added with (nolock) to avoid locking issue per rsd
		where nboc_prospect_id = @prospect_id and nboc_prop_num = @prop_num
	ELSE
		select @acct_enrl_nbr = acct_enrl_nbr from case_xref with (nolock)
		where nb_prospect_id = @prospect_id and nb_prop_num = @prop_num


	DECLARE tripleoption_plan_cursor CURSOR
	FOR
	select  bp.ben_plan_cd, eba.ben_agmt_nbr, 'P'
			from	enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock),
				bnftplan bp with(nolock)
			where 	eba.sgrs_plan_num = cpo.med_plan_id
			and 	bp.ben_plan_cd = eba.sgrs_plan_num
			and 	eba.acct_enrl_nbr=@ACCT_ENRL_NBR
			and 	cpo.rate_id = @rate_id and cpo.prem_type='Q'
-- 			and 	((bp.ben_plan_eff_dt <= @acct_eff_dt)
-- 			and 	(@acct_eff_dt <= bp.ben_plan_exp_dt))
			and 	bp.plantype_cd <> 'Q'
	union

	select  hmp.plan_id, eba.ben_agmt_nbr, 'H'
			from 	hmo_med_plan hmp with(nolock),
				enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock)
			where   hmp.PLAN_ID = eba.SGRS_PLAN_NUM
			and 	eba.ACCT_ENRL_NBR = @ACCT_ENRL_NBR
			and 	cpo.rate_id = @rate_id
			and 	cpo.prem_type='Q'
			and 	cpo.med_plan_id = eba.sgrs_plan_num
-- 			and 	((hmp.hmo_plan_desc_eff_date <= @acct_eff_dt)
-- 			and 	(@acct_eff_dt <= hmp.hmo_plan_desc_exp_date))


	OPEN tripleoption_plan_cursor

	FETCH NEXT FROM tripleoption_plan_cursor
	INTO @ben_plan_cd, @ben_agmt_nbr1, @hlth_cvg_typ

	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @coverage_type = 'O'
		EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, 'X', @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
		 @cnt = @eo_num output
--		usp_get_emp_cnt @nb_prospect_id, @nb_prop_num, @nb_rate_id, @coverage_type, @plan_type, @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr,
		set @coverage_type = 'C'
		EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, 'X', @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
		 @cnt = @ec_num output

		set @coverage_type = 'S'
		EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, 'X', @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
		 @cnt = @es_num output

		set @coverage_type = 'F'
		EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, 'X', @nb_ind, @hlth_cvg_typ, @ben_agmt_nbr1,
		 @cnt = @ef_num output

		update cvg_prem_out
		set total_premium_amt = ((EO_CVG_AMT    * @eo_num)
				   +(EC_E1_CVG_AMT * @ec_num)
				   +(ES_CVG_AMT    * @es_num)
				   +(EF_CVG_AMT    * @ef_num))
-- 		SELECT med_plan_id,
-- 			((EO_CVG_AMT    * @eo_num)
-- 				   +(EC_E1_CVG_AMT * @ec_num)
-- 				   +(ES_CVG_AMT    * @es_num)
-- 				   +(EF_CVG_AMT    * @ef_num)) as 'total_premium' ,
-- 			@eo_num as 'eo_vol',
-- 			eo_cvg_amt,
-- 			@ec_num as 'ec_vol',
-- 			ec_e1_cvg_amt,
-- 			@es_num as 'es_vol',
-- 			es_cvg_amt,
-- 			@ef_num as 'ef_vol',
-- 			ef_cvg_amt
		FROM cvg_prem_out with(NOLOCK)
		where  rate_id = @rate_id
			and    MED_PLAN_ID = @ben_plan_cd
			and    prem_type   = 'Q'


		FETCH NEXT FROM tripleoption_plan_cursor INTO @ben_plan_cd, @ben_agmt_nbr1, @hlth_cvg_typ
	END

	CLOSE tripleoption_plan_cursor

	DEALLOCATE tripleoption_plan_cursor

   end  -- BDB 04052007 end of changes
/*CKM20041022 end new business dual PPO section */
else
  begin
	--In case of SGRS (@nb_ind is 0) hlth_cvg_typ is ignored and both sets of counts are the same for a MOP

	--reset count variables to zero!
	select 	@eo_num = 0,
		@ec_num = 0,
		@es_num = 0,
		@ef_num = 0

	/*** PPO counts ***/
	set @hlth_cvg_typ = 'P'
	set @coverage_type = 'O'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ,
	 @cnt = @eo_num output

	set @coverage_type = 'C'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ,
	 @cnt = @ec_num output

	set @coverage_type = 'S'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ,
	 @cnt = @es_num output

	set @coverage_type = 'F'
	EXEC usp_get_emp_cnt @prospect_id, @prop_num, @rate_id, @coverage_type, @Plantype, @nb_ind, @hlth_cvg_typ,
	 @cnt = @ef_num output

	--CKM20041022 Now we have the counts needed to update cvg_prem_out totals for the second plan for dual PPO

	update cvg_prem_out
	set    total_premium_amt = ((EO_CVG_AMT    * @eo_num)
				   +(EC_E1_CVG_AMT * @ec_num)
				   +(ES_CVG_AMT    * @es_num)
				   +(EF_CVG_AMT    * @ef_num))
	where  rate_id = @rate_id
	and    plantype_cd in ('1','Z')
	and    prem_type   = 'Q'

   end
/**************************************************************************************
CKM20041022 - End of PPO total calculation
***************************************************************************************/

if @nb_ind = 1

  begin
	--SMK 060908
	IF (@NBOC_IND ='Y')
		select distinct  -- BDB20070518 Added for new prelims functionality.
		@quote_prospect_id =
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_prospect_id
				else quote_prospect_id end,
		@quote_prop_num = 
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_prop_num_id
				else quote_prop_num end,
		@quote_rate_id = 
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_rate_id
				else quote_rate_id end,
		@ACCT_ENRL_NBR = cx.acct_enrl_nbr
	from case_xref cx with (nolock) inner join
	pros_prop_out ppo on cx.quote_prospect_id = ppo.prospect_id -- 2007-07-23 CDZ - add pros_prop_out to from clause to get corp_ent_cd - distinct required
	 where nboc_prospect_id = @prospect_id --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
	ELSE

	--ECJ20030807 Need quote info. for adding the 'O' row
	select distinct  -- BDB20070518 Added for new prelims functionality.
		@quote_prospect_id =
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_prospect_id
				else quote_prospect_id end,
		@quote_prop_num = 
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_prop_num_id
				else quote_prop_num end,
		@quote_rate_id = 
			case when corp_ent_Cd = 'TX1' and stage_2_rate_id is not null and stage_2_rate_id > 0
				then stage_2_rate_id
				else quote_rate_id end,
		@ACCT_ENRL_NBR = cx.acct_enrl_nbr
	from case_xref cx with (nolock) inner join
	pros_prop_out ppo on cx.quote_prospect_id = ppo.prospect_id -- 2007-07-23 CDZ - add pros_prop_out to from clause to get corp_ent_cd - distinct required
	 where nb_prospect_id = @prospect_id --rr 0729/2005 added with (nolock) to avoid locking issue per rsd

/********* FIRST ADD THE SOLD 'S' ROW *****************/


	---save plan type to restore later
	set @Plantype_wk = @Plantype
	set @Plantype_o = @Plantype

	select @ipdpCD = ridercode from prop_riders with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
	where prospect_id = @prospect_id
	and prop_num = @prop_num
	and ridertype = 'RX'


if @Plantype in ('N','O')  -- BDB 03192007  Start Changes /Section 1
BEGIN

--	exec usp_delete_quoted_or_sold_rates @prospect_id,@prop_num,'S',@rate_id,'1'   --@hp_ind parameter usp_delete_quoted_or_sold_rates for tripleoption req_id#5507
-- 08/25/2009 Rdibba Added this code to remove the plan_type_cd dependency while deleting the 's' and 'o' rates

	DELETE FROM CVG_PREM_OUT   WITH(ROWLOCK)
	WHERE 	PROSPECT_ID = @PROSPECT_ID
	AND	PROP_NUM = @PROP_NUM
	AND	PREM_TYPE = 'S'
	AND	RATE_ID = @RATE_ID





	DECLARE tripleoption_plan_cursor CURSOR
	FOR
	select  bp.ben_plan_cd,cpo.EO_CVG_AMT,cpo.EC_E1_CVG_AMT,cpo.ES_CVG_AMT,cpo.EF_CVG_AMT, cpo.total_premium_amt, '1'
			from	enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock),
				bnftplan bp with(nolock)
			where 	eba.sgrs_plan_num = cpo.med_plan_id
			and 	bp.ben_plan_cd = eba.sgrs_plan_num
			and 	eba.acct_enrl_nbr=@ACCT_ENRL_NBR
			and 	cpo.rate_id = @rate_id and cpo.prem_type='Q'
			and 	((bp.ben_plan_eff_dt <= @acct_eff_dt)
			and 	(@acct_eff_dt <= bp.ben_plan_exp_dt))
			and 	bp.plantype_cd <> 'Q'
	union

	select  hmp.plan_id,cpo.EO_CVG_AMT,cpo.EC_E1_CVG_AMT,cpo.ES_CVG_AMT,cpo.EF_CVG_AMT,cpo.total_premium_amt, 'I'
			from 	hmo_med_plan hmp with(nolock),
				enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock)
			where   hmp.PLAN_ID = eba.SGRS_PLAN_NUM
			and 	eba.ACCT_ENRL_NBR = @ACCT_ENRL_NBR
			and 	cpo.rate_id = @rate_id
			and 	cpo.prem_type='Q'
			and 	cpo.med_plan_id = eba.sgrs_plan_num
			and 	((hmp.hmo_plan_desc_eff_date <= @acct_eff_dt)
			and 	(@acct_eff_dt <= hmp.hmo_plan_desc_exp_date))

	OPEN tripleoption_plan_cursor

	FETCH NEXT FROM tripleoption_plan_cursor
	INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total,@hp_ind

	WHILE @@FETCH_STATUS = 0
	BEGIN

	   	exec usp_insert_quoted_or_sold_rates @prospect_id,@prop_num,@ben_plan_cd,@eo,@ec,@es,@ef,@total,'S',@Userid, @rate_id, @hp_ind

		FETCH NEXT FROM tripleoption_plan_cursor INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total, @hp_ind
	END

	CLOSE tripleoption_plan_cursor

	DEALLOCATE tripleoption_plan_cursor


END    -- BDB 03192007 end changes /section 1
ELSE
BEGIN
	/*** Just retrieve the exploded rates from cvg_prem_out ***/
	if @Plantype in ('1','M','Z')
	    begin

		select 	@plan_id=bp.ben_plan_cd,
		       	@eo=cpo.EO_CVG_AMT,
		       	@ec=cpo.EC_E1_CVG_AMT,
		       	@es=cpo.ES_CVG_AMT,
		       	@ef=cpo.EF_CVG_AMT,
		       	@total=cpo.total_premium_amt
		from 	enrl_ben_agmt eba with(nolock),
			cvg_prem_out cpo with(nolock),
			bnftplan bp with(nolock)
		where 	eba.sgrs_plan_num = cpo.med_plan_id
		and 	bp.ben_plan_cd = eba.sgrs_plan_num
		and 	eba.acct_enrl_nbr=@ACCT_ENRL_NBR
		and 	cpo.rate_id = @rate_id
		and 	cpo.prem_type='Q'
		and 	((bp.ben_plan_eff_dt <= @acct_eff_dt)
		and 	(@acct_eff_dt <= bp.ben_plan_exp_dt))
		and 	bp.plantype_cd <> 'Q'

		exec usp_delete_quoted_or_sold_rates @prospect_id,@prop_num,'S',@rate_id, '1'
		-- ST 11-08-2005
	   	exec usp_insert_quoted_or_sold_rates @prospect_id,@prop_num,@plan_id,@eo,@ec,@es,@ef,@total,'S',@Userid, @rate_id, '1'  ---rsd 10/23/2002
	    end

	if @Plantype = 'X' --'X' for dual option PPO
	    begin
		exec usp_delete_quoted_or_sold_rates @prospect_id,@prop_num,'S',@rate_id, '1'

		--Declare a cursor to loop thru both plans within Dual PPO


                DECLARE dual_plan_cursor CURSOR FOR

		select  bp.ben_plan_cd,
		        cpo.EO_CVG_AMT,
		        cpo.EC_E1_CVG_AMT,
		        cpo.ES_CVG_AMT,
		        cpo.EF_CVG_AMT,
		        cpo.total_premium_amt
		from	enrl_ben_agmt eba with(nolock),
			cvg_prem_out cpo with(nolock),
			bnftplan bp with(nolock)
		where 	eba.sgrs_plan_num = cpo.med_plan_id
		and 	bp.ben_plan_cd = eba.sgrs_plan_num
		and 	eba.acct_enrl_nbr=@ACCT_ENRL_NBR
		and 	cpo.rate_id = @rate_id and cpo.prem_type='Q'
		and 	((bp.ben_plan_eff_dt <= @acct_eff_dt)
		and 	(@acct_eff_dt <= bp.ben_plan_exp_dt))
		and 	bp.plantype_cd <> 'Q'

		OPEN dual_plan_cursor

		FETCH NEXT FROM dual_plan_cursor
		INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total

		WHILE @@FETCH_STATUS = 0
		BEGIN

		   	exec usp_insert_quoted_or_sold_rates @prospect_id,@prop_num,@ben_plan_cd,@eo,@ec,@es,@ef,@total,
				'S',@Userid, @rate_id, '1'

			FETCH NEXT FROM dual_plan_cursor
			INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total
		END --CURSOR LOOP

		CLOSE dual_plan_cursor
		DEALLOCATE dual_plan_cursor
	    end --Plantype 'X' section for Dual PPO


	if @Plantype in ('I','M','Y')
	   begin
		select  @plan_id = hmp.plan_id, -- ST 11-08-2005 changing base_plan_desc to plan_id to get unique identifier
		        @eo=cpo.EO_CVG_AMT,
		        @ec=cpo.EC_E1_CVG_AMT,
		        @es=cpo.ES_CVG_AMT,
		        @ef=cpo.EF_CVG_AMT,
		        @total=cpo.total_premium_amt
		from 	hmo_med_plan hmp with(nolock),
			enrl_ben_agmt eba with(nolock),
			cvg_prem_out cpo with(nolock)
		where   hmp.PLAN_ID = eba.SGRS_PLAN_NUM
		and 	eba.ACCT_ENRL_NBR = @ACCT_ENRL_NBR
		and 	cpo.rate_id = @rate_id
		and 	cpo.prem_type='Q'
		and 	cpo.med_plan_id = eba.sgrs_plan_num
		and 	((hmp.hmo_plan_desc_eff_date <= @acct_eff_dt)
		and 	(@acct_eff_dt <= hmp.hmo_plan_desc_exp_date))

		exec usp_delete_quoted_or_sold_rates @prospect_id,@prop_num,'S',@rate_id, 'I' --ECJ20030227
		-- ST 11-08-2005
	   	exec usp_insert_quoted_or_sold_rates @prospect_id,@prop_num,@plan_id,@eo,@ec,@es,@ef,@total,'S',@Userid, @rate_id, 'I'  ---rsd 10/23/2002
	    end

END

/******** NEXT ADD THE ORIGINAL 'O' ROW ********************/

	Select @plan_id =''
	Select @eo=0
	Select @ec=0
	select @es=0
	Select @ef=0
	Select @total=0
	Select @hp_ind = ''
	---save plan type to restore later
	set @Plantype_wk = @Plantype_o

	select @ipdpCD = ridercode from prop_riders_out with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
	where prospect_id = @quote_prospect_id
	and prop_num = @quote_prop_num
	and rate_id = @quote_rate_id
	and ridertype = 'RX'


if @Plantype in ('N','O')  -- Start Changes /section 2
BEGIN
	exec usp_delete_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,'O',@quote_rate_id, '1'
	exec usp_delete_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,'O',@quote_rate_id, 'I'

	DECLARE tripleoption_plan_cursor CURSOR
	FOR
	select  bp.ben_plan_cd,cpo.EO_CVG_AMT,cpo.EC_E1_CVG_AMT,cpo.ES_CVG_AMT,cpo.EF_CVG_AMT, cpo.total_premium_amt, '1'
			from	enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock),
				bnftplan bp with(nolock)
			where 	eba.sgrs_plan_num = cpo.med_plan_id
			and 	bp.ben_plan_cd = eba.sgrs_plan_num
			and 	eba.acct_enrl_nbr=@ACCT_ENRL_NBR
			and 	cpo.rate_id = @quote_rate_id and cpo.prem_type='Q'
			and 	((bp.ben_plan_eff_dt <= @acct_eff_dt)
			and 	(@acct_eff_dt <= bp.ben_plan_exp_dt))
			and 	bp.plantype_cd <> 'Q'
	union

	select  hmp.plan_id,cpo.EO_CVG_AMT,cpo.EC_E1_CVG_AMT,cpo.ES_CVG_AMT,cpo.EF_CVG_AMT,cpo.total_premium_amt, 'I'
			from 	hmo_med_plan hmp with(nolock),
				enrl_ben_agmt eba with(nolock),
				cvg_prem_out cpo with(nolock)
			where   hmp.PLAN_ID = eba.SGRS_PLAN_NUM
			and 	eba.ACCT_ENRL_NBR = @ACCT_ENRL_NBR
			and 	cpo.rate_id = @quote_rate_id
			and 	cpo.prem_type='Q'
			and 	cpo.med_plan_id = eba.sgrs_plan_num
			and 	((hmp.hmo_plan_desc_eff_date <= @acct_eff_dt)
			and 	(@acct_eff_dt <= hmp.hmo_plan_desc_exp_date))

	OPEN tripleoption_plan_cursor

	FETCH NEXT FROM tripleoption_plan_cursor
	INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total,@hp_ind

	WHILE @@FETCH_STATUS = 0
	BEGIN

	   	exec usp_insert_quoted_or_sold_rates @quote_prospect_id, @quote_prop_num,@ben_plan_cd,@eo,@ec,@es,@ef,@total,'O',@Userid, @quote_rate_id, @hp_ind

		FETCH NEXT FROM tripleoption_plan_cursor INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total, @hp_ind
	END

	CLOSE tripleoption_plan_cursor

	DEALLOCATE tripleoption_plan_cursor


END		-- BDB 03192007 end changes /section 2
ELSE
BEGIN
	if @Plantype_o in ('1','M','Z')
	    begin

		Select @plan_id=''
		select @plan_id=bp.ben_plan_cd,
		       @eo=cpo.EO_CVG_AMT,
		       @ec=cpo.EC_E1_CVG_AMT,
		       @es=cpo.ES_CVG_AMT,
		       @ef=cpo.EF_CVG_AMT,
		       @total=cpo.total_premium_amt
		from   enrl_ben_agmt eba with(nolock),
		       cvg_prem_out cpo with(nolock),
		       bnftplan bp with(nolock)
		where  eba.sgrs_plan_num = cpo.med_plan_id
		and    bp.ben_plan_cd = eba.sgrs_plan_num
		and    eba.acct_enrl_nbr=@ACCT_ENRL_NBR
		and    cpo.rate_id = @quote_rate_id
		and    cpo.prem_type='Q'
		and    ((bp.ben_plan_eff_dt <= @acct_eff_dt)
		and    (@acct_eff_dt <= bp.ben_plan_exp_dt))
		and    bp.plantype_cd <> 'Q'
		/*CKM20041104 ENRL can now change the ben_plan_name and effective and so if they do, sometimes that new
		plan will not have been rated therefore we will not find a match to write the 'O' row. In that case, don't
		insert the 'O' row and go to reselling. If the nb selected plan was not rated the join above will fail*/
		if @@rowcount > 0
		   begin
			exec usp_delete_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,'O',@quote_rate_id, '1' --ECJ20030227
			-- ST 11-08-2005
	   		exec usp_insert_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,@plan_id,@eo,@ec,@es,@ef,@total,'O',@Userid, @quote_rate_id, '1'  ---rsd 10/23/2002
	     	   end
	   end

	if @Plantype_o = 'X' --ECJ20030303 Add 'X' for dual option PPO
	    begin
		exec usp_delete_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,'O',@quote_rate_id, '1'

		--have to declare a cursor to loop thru both plans within Dual PPO
                DECLARE dual_plan_cursor CURSOR FOR

		select bp.ben_plan_cd,
		       cpo.EO_CVG_AMT,
		       cpo.EC_E1_CVG_AMT,
		       cpo.ES_CVG_AMT,
		       cpo.EF_CVG_AMT,
		       cpo.total_premium_amt
		from   enrl_ben_agmt eba with(nolock),
		       cvg_prem_out cpo with(nolock),
                       bnftplan bp with(nolock)
		where
		       eba.sgrs_plan_num = cpo.med_plan_id
                and    bp.ben_plan_cd = eba.sgrs_plan_num
		and    eba.acct_enrl_nbr=@ACCT_ENRL_NBR
		and    cpo.rate_id = @quote_rate_id and cpo.prem_type='Q'
		and    ((bp.ben_plan_eff_dt <= @acct_eff_dt)
		and    (@acct_eff_dt <= bp.ben_plan_exp_dt))
		and    bp.plantype_cd <> 'Q'

		OPEN dual_plan_cursor

		FETCH NEXT FROM dual_plan_cursor
		INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total

		WHILE @@FETCH_STATUS = 0
		BEGIN
		   	exec usp_insert_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,@ben_plan_cd,@eo,@ec,@es,@ef,@total,
			'O',@Userid, @quote_rate_id, '1'

			FETCH NEXT FROM dual_plan_cursor
			INTO @ben_plan_cd,@eo,@ec,@es,@ef,@total
		END --CURSOR LOOP

		CLOSE dual_plan_cursor
		DEALLOCATE dual_plan_cursor
	    end --Plantype 'X' section for Dual PPO

	if @Plantype_o in ('I','M','Y')
	    begin
		Select @plan_id =''
		Select @eo=0
		Select @ec=0
		select @es=0
		Select @ef=0
		Select @total=0

		select @plan_id = hmp.plan_id,
		       @eo=cpo.EO_CVG_AMT,
		       @ec=cpo.EC_E1_CVG_AMT,
		       @es=cpo.ES_CVG_AMT,
		       @ef=cpo.EF_CVG_AMT,
		       @total=cpo.total_premium_amt
		from   hmo_med_plan hmp with(nolock),
		       enrl_ben_agmt eba with(nolock),
                       cvg_prem_out cpo with(nolock)
		where
		       hmp.PLAN_ID = eba.SGRS_PLAN_NUM
		and    eba.ACCT_ENRL_NBR = @ACCT_ENRL_NBR
		and    cpo.rate_id = @quote_rate_id
		and    cpo.prem_type='Q'
		and    cpo.med_plan_id = eba.sgrs_plan_num
		and    ((hmp.hmo_plan_desc_eff_date <= @acct_eff_dt)
		and    (@acct_eff_dt <= hmp.hmo_plan_desc_exp_date))

		/*CKM20041104 ENRL can now change the ben_plan_name and effective and so if they do, sometimes that new
		plan will not have been rated therefore we will not find a match to write the 'O' row. In that case, don't
		insert the 'O' row and go to reselling. If the nb selected plan was not rated the join above will fail*/
		if @@rowcount > 0
		   begin
			exec usp_delete_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,'O',@quote_rate_id, 'I' --ECJ20030227
			-- ST 11-08-2005
	   		exec usp_insert_quoted_or_sold_rates @quote_prospect_id,@quote_prop_num,@plan_id,@eo,@ec,@es,@ef,@total,'O',@Userid, @quote_rate_id, 'I'  ---rsd 10/23/2002
		   end
	    end
END

/**********DONE ADDING SOLD AND ORIGINAL ROWS **************************/

  --ECJ 2002-06-21 Expand new business section to include call to usp_census_hlth_prem
if @Plantype = 'M'  ---rsd 10/25/2002 added plantype = "M" for MOP.
	    begin
		if  (@acct_eff_dt >= @date_factor) --CKM20040929 added this if block for 1/1/05 efft mops
		    begin
		       	select @iplanCD = ben_plan_cd
			from pros_prop_out  with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
			where rate_id = @rate_id
		    end
		else
		    begin
			select @iplanCD = med_plan_id from cvg_prem_out with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
			where prospect_id = @prospect_id
			and prop_num = @prop_num
			and prem_type = 'S'
			and plantype_cd = '1'
			and rate_id = @rate_id  ---rsd 08/09/2002
		     end

		---rsd 11/13/2002 added code to get @iHmoPlanCd.
		---this value is needed for the HMO portion of a MOP.
		select @iHmoPlanCd = med_plan_id from cvg_prem_out with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
		where prospect_id = @prospect_id
		and prop_num =  @prop_num
		and prem_type = 'S'
		and plantype_cd = 'I'
		and rate_id = @rate_id
	    end
	else if @Plantype = 'X' --ECJ20030320 added plantype 'X' for Dual Option PPO
	    --CKM 07/02/2004 Added effective data to filter out inactive records.
	    begin
		select 	@iplanCD = bpx.mult_plan_cd
		from 	bnftplan_xref bpx with (nolock),
			cvg_prem_out co with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
		where 	co.med_plan_id = bpx.ben_plan_cd
		and	co.prospect_id = @prospect_id
		and 	co.prop_num =  @prop_num
		and 	co.prem_type = 'S'
		and 	co.plantype_cd = '1'
		and 	co.rate_id = @rate_id
		and 	((bpx.eff_date  <= @acct_eff_dt) and (@acct_eff_dt <= bpx.exp_date))
		group by bpx.mult_plan_cd
		having count(bpx.mult_plan_cd) >1


		set @iHmoPlanCd = 0 -- this value is only used for MOP
	    end
	else
	    begin
		select @iplanCD = med_plan_id from cvg_prem_out with (nolock) --rr 0729/2005 added with (nolock) to avoid locking issue per rsd
		where prospect_id = @prospect_id
		and prop_num =  @prop_num
		and prem_type = 'S'
		and rate_id = @rate_id  ---rsd 08/09/2002

		set @iHmoPlanCd = 0  ---rsd 11/13/2002 this value is only used for MOP.
	    end


	exec usp_census_hlth_prem @prospect_id,@prop_num,@iplanCD,@iHmoPlanCd,@ipdpCD, @rate_id,1  ---rsd 08/09/2002

		--CKM20041112 Insert HMO sold rows into uw_prem_out
		--First delete incase this proc gets called twice for the same case
		delete 	from uw_prem_out
		where 	prospect_id 	= @prospect_id
		and 	prop_num 	= @prop_num
		and 	rate_id 	= @rate_id

		--CKM get the carrier_cd for tx, the current carrier_cd is 6
		set @carrier_cd    =   (select distinct carrier_cd
				        from hmo_service_area
				        where hmo_cd = 'I' 
				        and corp_ent_cd = 'TX1'
				        and @acct_eff_dt between serv_area_eff_date and serv_area_exp_date )
		--CKM get the carrier_plan_cd
		set @carrier_plan_cd = (select carrier_plan_cd
					from carrier_plan_xref
					where @acct_eff_dt between carrier_plan_eff_dt and carrier_plan_exp_dt
					and corp_ent_cd = 'tx1'
					and benefit_type = 'medical' )


	 	insert into uw_prem_out(
			prospect_id,
			prop_num,
			carrier_cd,
			carrier_plan_cd,
			benefit_type,
			plan_id,
			eo_cvg_amt,
			ec_cvg_amt,
			es_cvg_amt,
			ef_cvg_amt,
			modified_by,
			modified_ts,
			corp_ent_cd,
			rate_id)
	     	select
			prospect_id,
			prop_num,
			@carrier_cd,
			@carrier_plan_cd,
			'medical',
			MED_PLAN_ID,
			EO_CVG_AMT,
			EC_E1_CVG_AMT,
			ES_CVG_AMT,
			EF_CVG_AMT,
			@Userid,
			getdate(),
			CORP_ENT_CD,
			rate_id
		from	cvg_prem_out
		where 	prospect_id = @prospect_id
		and 	prop_num = @prop_num
		and 	rate_id = @rate_id
		and 	plantype_cd = 'I'
		and 	prem_type = 'S'
		--CKM20041112 done inserting HMO sold rows into uw_prem_out

		--CKM20041115 Now insert selected HMO riders
		--CKM20041208 Added a 1 to 1 join on carrier_plan_xref to eliminate the ppssibility of having a null for carrier_plan_cd
		insert into uw_prem_out(
			prospect_id,
			prop_num,
			carrier_cd,
			carrier_plan_cd,
			benefit_type,
			plan_id,
			eo_cvg_amt,
			ec_cvg_amt,
			es_cvg_amt,
			ef_cvg_amt,
			modified_by,
			modified_ts,
			corp_ent_cd,
			rate_id)
	     	select
			pro.prospect_id,
			pro.prop_num,
			@carrier_cd,
			--@carrier_plan_cd,
			cpx.carrier_plan_cd ,
			pro.ridertype,
			0,
			0,
			0,
			0,
			0,
			@Userid,
			getdate(),
			pro.CORP_ENT_CD,
			pro.rate_id
		from	prop_riders_out pro with(nolock),
			carrier_plan_xref cpx with(nolock)
		where 	pro.prospect_id = @prospect_id
		and 	pro.prop_num = @prop_num
		and 	pro.rate_id = @rate_id
		and 	pro.plantype in ('I','M','Y')
		and 	pro.ridercode <> 'N'
		and 	 @acct_eff_dt between cpx.carrier_plan_eff_dt and cpx.carrier_plan_exp_dt
		and 	cpx.corp_ent_cd = pro.corp_ent_cd
		and 	cpx.benefit_type = pro.ridertype

		--CKM20041115 Done inserting selected HMO riders
	--SMK060908
	IF (@NBOC_IND ='Y')
	BEGIN
		update case_xref
  		set nboc_rate_id = @rate_id
        	where nboc_prospect_id = @prospect_id and
	       	nboc_prop_num = @prop_num

		update nboc_mod_hist
  		set nboc_rate_id = @rate_id ,
		SKP_RTG_IND= 'N' 		/*SMK 090908*/
        	where nboc_prospect_id = @prospect_id and
	       	nboc_prop_num = @prop_num
	END
	ELSE
	---rsd 10/23/2002 update enrl_acct and case_xref with new rate_id
        	update case_xref
  		set nb_rate_id = @rate_id
        	where nb_prospect_id = @prospect_id and
	       	nb_prop_num = @prop_num

     end
 end
/* end section to add sold rates for new business */
